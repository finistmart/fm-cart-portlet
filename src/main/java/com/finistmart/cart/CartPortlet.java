package com.finistmart.cart;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.finistmart.cart.i18n.Utf8ResourceBundleControl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;

import java.net.URLDecoder;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;


import javax.portlet.*;
import javax.servlet.ServletRequest;
import javax.servlet.ServletRequestWrapper;


public class CartPortlet extends GenericPortlet {

	private static final Logger logger = LoggerFactory.getLogger(CartPortlet.class);

	ObjectMapper jsonMapper = new ObjectMapper();

	OrdersService ordersService;

	HttpOrdersService httpOrdersService;

	MailService mailService;

	boolean ordersLoaded;

	static Map<String, PortletSetting> settings = new HashMap<String, PortletSetting>() {{
		put("serviceUploadEnabled", new PortletSetting("serviceUploadEnabled", "boolean", false));
		put("serviceUrl", new PortletSetting("serviceUrl", "text", "cmis://some.docssystem.com/Path/fileName.csv"));
		put("serviceLogin", new PortletSetting("serviceLogin", "text", "user"));
		put("servicePassword", new PortletSetting("servicePassword", "text", "123456"));
		put("serviceType", new PortletSetting("serviceType", "text", "cmis"));
		put("notificationEmailEnabled", new PortletSetting("notificationEmailEnabled", "boolean", false));
		put("notificationEmail", new PortletSetting("serviceType", "text", "root@localhost"));
		put("notificationEmailSMTPProtocol", new PortletSetting("notificationEmailProtocol", "text", "smtps"));
		put("notificationEmailSMTPHost", new PortletSetting("notificationEmailSMTPHost", "text", "smtp.mailhost.com"));
		put("notificationEmailSMTPPort", new PortletSetting("notificationEmailSMTPPort", "text", "465"));
		put("notificationEmailSMTPFrom", new PortletSetting("notificationEmailSMTPFrom", "text", "no-reply@maildomain.com"));
		put("notificationEmailSMTPAuth", new PortletSetting("notificationEmailSMTPAuth", "text", "true"));
		put("notificationEmailSMTPDebug", new PortletSetting("notificationEmailSMTPDebug", "text", "true"));
		put("notificationEmailSMTPUser", new PortletSetting("notificationEmailSMTPUser", "text", "mail@maildomain.com"));
		put("notificationEmailSMTPPassword", new PortletSetting("notificationEmailSMTPPassword", "text", "secret"));
		put("notificationEmailSMTPReplyAddress", new PortletSetting("notificationEmailSMTPReplyAddress", "text", "no-reply@maildomain.com"));
		put("notificationEmailSMTPReplyPersonal", new PortletSetting("notificationEmailSMTPReplyPersonal", "text", "No Reply"));
		put("alternativeSelector", new PortletSetting("alternativeSelector", "text", ""));
		put("alternativeSelectorInsert", new PortletSetting("alternativeSelectorInsert", "text", "beforeend"));
		put("basketBtnSticky", new PortletSetting("basketBtnSticky", "text", "false"));
		put("basketBtnStickyOffset", new PortletSetting("basketBtnStickyOffset", "text", "10px"));
	}};

	@Override
	public void init() throws PortletException {
		this.mailService = new MailService();
	}

	public String extParam(String value) throws UnsupportedEncodingException {
		return URLDecoder.decode(new String(Base64.getDecoder().decode(value)), "UTF-8");
	}

	public void serveResource(ResourceRequest request, ResourceResponse response) throws PortletException, IOException {
		if (request.getResourceID().equals("getSettings")) {

			response.setContentType("application/json");
			PortletPreferences portletPreferences = request.getPreferences();

			Map<String, Object> settingsResponse = new HashMap<String, Object>();

			for (Map.Entry entry : settings.entrySet()) {
				PortletSetting setting = (PortletSetting) entry.getValue();
				settingsResponse.put(entry.getKey().toString(), portletPreferences.getValue(entry.getKey().toString(), String.valueOf(setting.getDefValue())));
			}

			Map<String, String> translations = getTranslationsMap(request, response);
			settingsResponse.put("l11n", translations);

			PrintWriter out = response.getWriter();

			out.write(jsonMapper.writeValueAsString(settingsResponse));
			out.flush();
			out.close();
		}
	}

	private Map<String, String> getTranslationsMap(ResourceRequest request, ResourceResponse response) {
		Map<String, String> translations = new HashMap<>();
		ResourceBundle.Control utf8Control =
                new Utf8ResourceBundleControl();
		ResourceBundle resources = ResourceBundle.getBundle("locale/CartPortlet_" + response.getLocale().getLanguage(), request.getLocale(), utf8Control);
		Enumeration<String> key = resources.getKeys();

		while (key.hasMoreElements()) {
            String param = (String) key.nextElement();
            translations.put("tr." + param, resources.getString(param));
        }
		return translations;
	}

	private String getPrefValue(PortletPreferences portletPreferences, String key) {
		return portletPreferences.getValue(key, String.valueOf(settings.get(key).getDefValue()));
	}


	public void processAction(ActionRequest request, ActionResponse response) throws PortletException,
			java.io.IOException {

		ServletRequestWrapper wrapper = (ServletRequestWrapper) request.getAttribute("com.liferay.portal.kernel.servlet.PortletServletRequest");
		ServletRequest servletRequest = null;
		if (wrapper != null) {
			servletRequest = ((ServletRequestWrapper) (wrapper.getRequest())).getRequest();
		}

		PortletPreferences prefs = request.getPreferences();

		if ("placeOrder".equals(request.getParameter("javax.portlet.action"))) {

			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
			LocalDateTime localDateTime = LocalDateTime.now();

			String orderDate = localDateTime.format(dtf);
			String items = servletRequest != null
					? servletRequest.getParameter("items")
					: request.getActionParameters().getValue("items");
			String delivery = servletRequest != null
					? servletRequest.getParameter("delivery")
					: request.getActionParameters().getValue("delivery");
			String userId = request.getRemoteUser() != null ? request.getRemoteUser() : "anonymous";
			String[] data = new String[] { orderDate, items, delivery };
			Map<String, String> dataMap = new HashMap<String, String>() {{
				put("order", jsonMapper.writeValueAsString(data));
			}};
			if ("cmis".equals(getPrefValue(prefs, "serviceType"))) {
				this.ordersService.addOrder(userId, data);
			} else {
				this.httpOrdersService.addOrder(userId, data, delivery);
			}

			// :TODO read all settings from group
			Properties properties = System.getProperties();

			properties.setProperty("mail.transport.protocol", getPrefValue(prefs, "notificationEmailSMTPProtocol"));
			properties.setProperty("mail.smtps.host", getPrefValue(prefs,"notificationEmailSMTPHost"));
			properties.setProperty("mail.smtps.port", getPrefValue(prefs,"notificationEmailSMTPPort"));
			properties.setProperty("mail.smtps.from", getPrefValue(prefs,"notificationEmailSMTPFrom"));
			properties.setProperty("mail.smtps.auth", getPrefValue(prefs,"notificationEmailSMTPAuth"));
			properties.setProperty("mail.debug", getPrefValue(prefs,"notificationEmailSMTPDebug"));
			properties.setProperty("mail.smtps.user", getPrefValue(prefs,"notificationEmailSMTPUser"));
			properties.setProperty("mail.smtps.password", getPrefValue(prefs,"notificationEmailSMTPPassword"));
			properties.setProperty("reply-to-address", getPrefValue(prefs,"notificationEmailSMTPReplyAddress"));
			properties.setProperty("reply-to-personal", getPrefValue(prefs,"notificationEmailSMTPReplyPersonal"));

			this.mailService.setProperties(properties);

			this.mailService.sendEmail(getPrefValue(prefs, "notificationEmail"), dataMap);
		} else if ("saveSettings".equals(request.getParameter("javax.portlet.action"))) {
			try {
				for (Map.Entry entry : settings.entrySet()) {
					PortletSetting setting = (PortletSetting) entry.getValue();
					String param = null;
					if (servletRequest != null) {
						param = extParam(servletRequest.getParameter(entry.getKey().toString()));
					} else {
						param = extParam(request.getActionParameters().getValue(entry.getKey().toString()));
					}
					if (param != null) {
						prefs.setValue(entry.getKey().toString(), param);
					}
				}
				prefs.store();
			} catch (ReadOnlyException e) {
				logger.error("Error saving readonly portlet preference", e);
			} catch (IOException e) {
				logger.error("Error reading/writing portlet preference", e);
			} catch (ValidatorException e) {
				logger.error("Error validating portlet preference", e);
			}
		}

	}

	protected void doView(RenderRequest renderRequest, RenderResponse renderResponse)
		throws PortletException, IOException {

		PortletPreferences portletPreferences = renderRequest.getPreferences();

		for (Map.Entry entry : settings.entrySet()) {
			PortletSetting setting = (PortletSetting) entry.getValue();
			renderRequest.setAttribute(entry.getKey().toString(), portletPreferences.getValue(entry.getKey().toString(), String.valueOf(setting.getDefValue())));
		}

		renderRequest.setAttribute("settings", settings.keySet());
        renderRequest.setAttribute("settingsJson", jsonMapper.writeValueAsString(settings));

		String serviceUrl = portletPreferences.getValue("serviceUrl", String.valueOf(settings.get("serviceUrl").getDefValue()));
		renderRequest.setAttribute("serviceUrl", serviceUrl);
		URI serviceURL = null;
		try {
			// :TODO do below better
			serviceURL = new URI(serviceUrl.replaceAll(" ","%20"));
		} catch (URISyntaxException e) {
			logger.error("Error reading url from portlet prefs", e);
		}

		boolean serviceUploadEnabled = Boolean.parseBoolean(portletPreferences.getValue("serviceUploadEnabled", String.valueOf(settings.get("serviceUploadEnabled").getDefValue())));
		if (serviceUploadEnabled && ! ordersLoaded) {
			String serviceType = portletPreferences.getValue("serviceType", String.valueOf(settings.get("serviceType").getDefValue()));
			String serviceLogin = portletPreferences.getValue("serviceLogin", String.valueOf(settings.get("serviceLogin").getDefValue()));
			String password = portletPreferences.getValue("servicePassword", String.valueOf(settings.get("servicePassword").getDefValue()));
			if ("cmis".equals(getPrefValue(portletPreferences, "serviceType"))) {
				ordersService = new OrdersService(serviceType, serviceLogin, password, serviceURL.getPath());
				ordersService.loadOrders();
				ordersLoaded = true;
			} else {
				httpOrdersService = new HttpOrdersService();
				httpOrdersService.setServiceUrl(serviceUrl);
				httpOrdersService.setServiceLogin(serviceLogin);
				httpOrdersService.setServicePassword(password);
			}
		}

		String viewPath = "/WEB-INF/views/cartViewMode.jsp";
		PortletRequestDispatcher portletRequestDispatcher = getPortletContext().getRequestDispatcher(viewPath);
		portletRequestDispatcher.include(renderRequest, renderResponse);
	}
}
