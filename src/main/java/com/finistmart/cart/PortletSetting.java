package com.finistmart.cart;

public class PortletSetting {

    public PortletSetting() {
    }

    public PortletSetting(String name, String dataType, Object defValue) {
        this.name = name;
        this.dataType = dataType;
        this.defValue = defValue;
    }

    String name;

    String dataType = "text";

    Object defValue;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public Object getDefValue() {
        return defValue;
    }

    public void setDefValue(Object defValue) {
        this.defValue = defValue;
    }
}
