package com.finistmart.cart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.time.LocalDateTime;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;



public class MailService {

    private static final Logger logger = LoggerFactory.getLogger(MailService.class);

    Properties properties;

    public MailService() {
    }

    public void sendEmail(String to, Map<String, String> data) {
        try {
            String subject = "New Order " + LocalDateTime.now();
            String htmlContent = data.entrySet()
                    .stream()
                    .map(entry -> entry.getKey() + " - " + entry.getValue())
                    .collect(Collectors.joining(", "));



            Authenticator authenticator = new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(properties.getProperty("mail.smtps.user"), properties.getProperty("mail.smtps.password"));
                }
            };
            Session session = Session.getInstance(properties, authenticator);
            MimeMessage message = new MimeMessage(session);

            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject(subject);
            message.setContent(htmlContent, "text/html");
            message.setFrom(new InternetAddress(properties.getProperty("reply-to-address"), properties.getProperty("reply-to-personal")));

            Transport trnsport;
            trnsport = session.getTransport("smtps");
            trnsport.connect(properties.getProperty("mail.smtps.host"), Integer.parseInt(properties.getProperty("mail.smtps.port")), properties.getProperty("mail.smtps.user"), properties.getProperty("mail.smtps.password"));
            message.saveChanges();
            trnsport.sendMessage(message, message.getAllRecipients());
            trnsport.close();
        } catch (Exception e) {
            logger.error("Error sending notification email", e.getMessage());
        }
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }
}

