package com.finistmart.cart;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.finistmart.cart.dto.Delivery;
import com.finistmart.cart.dto.OrdersAdd;
import org.apache.hc.client5.http.ClientProtocolException;

import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.ClassicHttpResponse;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.HttpStatus;
import org.apache.hc.core5.http.ParseException;
import org.apache.hc.core5.http.io.HttpClientResponseHandler;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.apache.hc.core5.http.io.entity.StringEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Base64;

public class HttpOrdersService {

    private static final Logger logger = LoggerFactory.getLogger(HttpOrdersService.class);

    private String serviceUrl = null;

    private String serviceLogin = null;

    private String servicePassword = null;

    private ObjectMapper jsonMapper = null;

    private ObjectMapper getMapper() {
        if (jsonMapper == null) {
            jsonMapper = new ObjectMapper();
        }
        return jsonMapper;
    }

    public String getAuthHash() {
        return Base64.getEncoder().encodeToString((serviceLogin + ":" + servicePassword).getBytes());
    }

    public void addOrder(String userId, String[] data, String deliveryJson) throws IOException {
        if (serviceUrl != null) {
            Delivery delivery = getMapper().readValue(deliveryJson, Delivery.class);
            logger.info("HttpOrdersService::addOrder: " + serviceUrl);
            try (final CloseableHttpClient httpclient = HttpClients.createDefault()) {
                String requestUrl = serviceUrl + "/api/orders/add";
                final HttpPost request = new HttpPost(requestUrl);
                if (serviceLogin != null) {
                    request.setHeader("Authorization", "Basic " + getAuthHash());
                }
                OrdersAdd ordersAdd = new OrdersAdd(getMapper().writeValueAsString(data));
                ordersAdd.setBuyerIdent(delivery.getEmail());
                ordersAdd.setAppIdent(userId);
                StringEntity entity = new StringEntity(getMapper().writeValueAsString(ordersAdd), Charset.forName("UTF-8"));
                request.setEntity(entity);
                request.setHeader("Accept", "application/json");
                request.setHeader("Content-type", "application/json");
                // Create a custom response handler
                final HttpClientResponseHandler<String> responseHandler = new HttpClientResponseHandler<String>() {

                    @Override
                    public String handleResponse(
                            final ClassicHttpResponse response) throws IOException {
                        final int status = response.getCode();
                        if (status >= HttpStatus.SC_SUCCESS && status < HttpStatus.SC_REDIRECTION) {
                            final HttpEntity entity = response.getEntity();
                            try {
                                return entity != null ? EntityUtils.toString(entity) : null;
                            } catch (final ParseException ex) {
                                throw new ClientProtocolException(ex);
                            }
                        } else {
                            throw new ClientProtocolException("Unexpected response status: " + status);
                        }
                    }

                };
                final String responseBody = httpclient.execute(request, responseHandler);
                //categories = getMapper().readValue(responseBody, new TypeReference<List<Category>>() { });

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    public String getServiceLogin() {
        return serviceLogin;
    }

    public void setServiceLogin(String serviceLogin) {
        this.serviceLogin = serviceLogin;
    }

    public String getServicePassword() {
        return servicePassword;
    }

    public void setServicePassword(String servicePassword) {
        this.servicePassword = servicePassword;
    }

    public ObjectMapper getJsonMapper() {
        return jsonMapper;
    }

    public void setJsonMapper(ObjectMapper jsonMapper) {
        this.jsonMapper = jsonMapper;
    }
}
