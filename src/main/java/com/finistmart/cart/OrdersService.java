package com.finistmart.cart;

import com.finistmart.CmisClient;
import com.opencsv.CSVWriter;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.exceptions.CmisObjectNotFoundException;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ContentStreamImpl;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.math.BigInteger;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class OrdersService {

    private static final Logger logger = LoggerFactory.getLogger(OrdersService.class);

    CmisClient cmisClient;

    private CSVWriter writer;
    private File tempFile;

    private String conName;
    private String login;
    private String password;
    private String docPath;

    OrdersService(String conName, String login, String password, String docPath) {
        this.conName = conName;
        this.login = login;
        this.password = password;
        this.docPath = docPath;
        cmisClient = new CmisClient();
    }

    void loadOrders() {
        Document ordersDocument;
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        Session session = cmisClient.getSession(conName, login, password);
        try {
            ordersDocument = (Document) session.getObjectByPath(docPath);
        } catch (CmisObjectNotFoundException ce) {
            String folderPath = Paths.get(docPath).getParent().toString();
            Folder folder = (Folder) session.getObjectByPath(folderPath);
            Map<String, String> fileProps = new HashMap<String, String>() {{
                put("cmis:name", "orders.csv");
                put("cmis:objectTypeId", "cmis:document");
            }};
            ContentStreamImpl contentStream = new ContentStreamImpl();
            contentStream.setFileName("orders.csv");
            contentStream.setStream(new ByteArrayInputStream(os.toByteArray()));
            contentStream.setLength(new BigInteger("5"));
            contentStream.setMimeType("text/csv");
            ordersDocument = folder.createDocument(fileProps, contentStream, VersioningState.MAJOR);
        }
        try {
            tempFile = File.createTempFile("orders", "csv");

            FileOutputStream out = new FileOutputStream(tempFile);
            IOUtils.copy(ordersDocument.getContentStream().getStream(), out);
            out.close();
            writer = new CSVWriter(new FileWriter(tempFile));
        } catch (IOException e) {
            System.out.println("Error witing temp file");
        }
    }

    void addOrder(String userId, String[] data) throws IOException {
        writer.writeNext(data);
        writer.flush();

        Session session = cmisClient.getSession(conName, login, password);
        Document ordersDocument = (Document) session.getObjectByPath(docPath);
        Map<String, String> fileProps = new HashMap<String, String>() {{
            put("cmis:name", "orders.csv");
            put("cmis:objectTypeId", "cmis:document");
        }};
        ContentStreamImpl contentStream = new ContentStreamImpl();
        contentStream.setFileName("orders.csv");
        contentStream.setStream(new ByteArrayInputStream(FileUtils.readFileToByteArray(tempFile)));
        //contentStream.setLength(new BigInteger("5"));
        contentStream.setMimeType("text/csv");
        ordersDocument.setContentStream(contentStream, true, true);
        logger.info("Updated online document contents: " + LocalDateTime.now() + ", with: " + Arrays.toString(data));
    }
}
