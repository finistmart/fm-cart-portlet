package com.finistmart.cart.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown=true)
public class SettingsResponse {

    private String serviceUrl;
    private List<Map<String, Object>> productData;
    private String template;

    public SettingsResponse() {
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    public List<Map<String, Object>> getProductData() {
        return productData;
    }

    public void setProductData(List<Map<String, Object>> productData) {
        this.productData = productData;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }
}
