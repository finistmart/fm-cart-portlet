<?xml version="1.0" encoding="UTF-8"?>
<%@ page import="java.util.Set" %>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<portlet:defineObjects />

<%
	String ctxPath = request.getContextPath();
%>


<portlet:actionURL var="saveSettingsURL" name="saveSettings"></portlet:actionURL>
<portlet:resourceURL var="getSettingsURL" id="getSettings"></portlet:resourceURL>
<portlet:actionURL var="placeOrderURL" name="placeOrder"></portlet:actionURL>


<style>
	dialog {
		position: absolute;
		left: 0; right: 0;
		width: -moz-fit-content;
		width: -webkit-fit-content;
		width: fit-content;
		height: -moz-fit-content;
		height: -webkit-fit-content;
		height: fit-content;
		margin: auto;
		border: solid;
		padding: 1em;
		background: white;
		color: black;
		display: block;
	}

	dialog:not([open]) {
		display: none;
	}

	dialog + .backdrop {
		position: fixed;
		top: 0; right: 0; bottom: 0; left: 0;
		background: rgba(0,0,0,0.1);
	}

	._dialog_overlay {
		position: fixed;
		top: 0; right: 0; bottom: 0; left: 0;
	}

	dialog.fixed {
		position: fixed;
		top: 50%;
		transform: translate(0, -50%);
	}

	.fm-btn-configure {
		background-image: url('<%= ctxPath %>/images/settings_black_192x192.png');
		background-size: contain;
		width: 50px;
		height: 50px;
		float: right;
		margin-top: -50px;
	}
	dialog:not([open]) {
		display: none !important;
	}
	.fm-cart-btn, .fm-cart-btn-icon {
		width: 100px;
		height: 100px;
		z-index: 900;
	}
	.fm-cart-cnt {
		position: relative;
		z-index: 200;
		bottom: 20px;
		left: 90px;
	}
	.fm-cart-field {
		padding: 0.4em;
	}
	.fm-cart-field label {
		min-width: 120px;
	}
	.fm-cart-empty .fm-cart-cnt {
		display: none;
	}
	.fm-cart-buttons {
		padding-top: 0.2em;
		padding-bottom: 0.2em;
	}
	.fm-form input:invalid, .fm-form input:focus:invalid {
		border: 1px solid red;
	}
	.fm-cart-added .fm-cart-btn {
		animation: shake 0.82s cubic-bezier(.36,.07,.19,.97) both;
		transform: translate3d(0, 0, 0);
		backface-visibility: hidden;
		perspective: 1000px;
	}
	.fm-cart-placeorder {
		float: right;
	}
	@keyframes shake {
		10%, 90% {
			transform: translate3d(-1px, 0, 0);
		}

		20%, 80% {
			transform: translate3d(2px, 0, 0);
		}

		30%, 50%, 70% {
			transform: translate3d(-4px, 0, 0);
		}

		40%, 60% {
			transform: translate3d(4px, 0, 0);
		}
	}
	dialog {
		border-width: initial;
		border-style: solid;
		border-color: initial;
		border-image: initial;
		padding: 1em;
		background: white;
	}
	dialog label {
		min-width: 250px;
	}
</style>
<%
	String alternativeSelector = (String) renderRequest.getAttribute("alternativeSelector");
	String alternativeSelectorInsert = (String) renderRequest.getAttribute("alternativeSelectorInsert");
	String basketBtnSticky = (String) renderRequest.getAttribute("basketBtnSticky");
	String basketBtnStickyOffset = (String) renderRequest.getAttribute("basketBtnStickyOffset");
	String portletSuffix = (String) renderResponse.getNamespace();
	portletSuffix = portletSuffix.substring(0, portletSuffix.length() - 1);
%>
<style>
	@media only screen and (max-width: 1000px) { /* mobile hide for liferay*/
		.fm-cart-btn, .fm-cart-btn-icon {
			width: 50px;
			height: 50px;
		}
	}
</style>

<%
	Set<String> settings = (Set<String>) renderRequest.getAttribute("settings");
	String settingsJson = (String) renderRequest.getAttribute("settingsJson");
%>

<template id="${renderResponse.getNamespace()}template-cart-list">
	<table>
		<thead>
			<tr>
				<th class="tr-cart-title-no">No</th>
				<th class="tr-cart-title-title">Title</th>
				<th class="tr-cart-title-price">Price</th>
				<th class="tr-cart-title-qty">Qty</th>
				<th class="tr-cart-title-actions">Actions</th>
			</tr>
		</thead>
		<tbody class="fm-cart-container">

		</tbody>
	</table>
	<button class="tr-cart-btn-buy fm-cart-buy" id="${renderResponse.getNamespace()}cartListBuyBtn">Buy</button>
</template>

<template id="${renderResponse.getNamespace()}template-cart-list-empty">
	<h2 class="tr-cart-label-no-items">No items.</h2>
</template>

<template id="${renderResponse.getNamespace()}template-cart-item">
	<tr class="fm-cart-item">
		<td class="fm-cart-no"></td>
		<td class="fm-cart-title"></td>
		<td class="fm-cart-price"></td>
		<td class="fm-cart-qty"></td>
		<td class="fm-cart-actions">
			<button class="tr-cart-action-remove action-remove">Remove</button>
		</td>
	</tr>
</template>

<template id="${renderResponse.getNamespace()}template-cart-button">
	<div class="fm-cart-btn" id="${renderResponse.getNamespace()}cartButton">
		<img class="fm-cart-btn-icon" src="<%= ctxPath %>/images/shopping-cart.png" />
		<span id="${renderResponse.getNamespace()}cartButtonItemCount" class="fm-cart-cnt"></span>
	</div>
</template>

<template id="${renderResponse.getNamespace()}template-delivery">
	<form class="fm-form" id="${renderResponse.getNamespace()}deliveryForm">
		<div class="fm-cart-field">
			<label class="tr-cart-label-email">Email</label>
			<sk-input type="email" required name="email"></sk-input>
		</div>
		<div class="fm-cart-field">
			<label class="tr-cart-label-country">Country</label>
			<select name="country">
				<option value="russia">Russia</option>
			</select>
		</div>
		<div class="fm-cart-field">
			<label class="tr-cart-label-province">Province/State</label>
			<sk-input type="text" name="state"></sk-input>
		</div>
		<div class="fm-cart-field">
			<label class="tr-cart-label-city">City</label>
			<sk-input type="text" name="city"></sk-input>
		</div>
		<div class="fm-cart-field">
			<label class="tr-cart-label-zipcode">Zipcode</label>
			<sk-input type="text" name="zipcode"></sk-input>
		</div>
		<div class="fm-cart-field">
			<label class="tr-cart-label-address1">Address1</label>
			<sk-input type="text" name="address1"></sk-input>
		</div>
		<div class="fm-cart-field">
			<label class="tr-cart-label-address2">Address2</label>
			<sk-input type="text" name="address2"></sk-input>
		</div>
		<div class="fm-cart-field">
			<label class="tr-cart-label-fullname">Full name</label>
			<sk-input type="text" required name="fullName"></sk-input>
		</div>
		<div class="fm-cart-field">
			<label class="tr-cart-label-tel">Tel</label>
			<sk-input type="number" name="tel"></sk-input>
		</div>
		<div class="fm-cart-buttons">
			<sk-button id="${renderResponse.getNamespace()}deliveryBackBtn" class="tr-cart-btn-cancel small cancel-modal-btn">Cancel</sk-button>
			<sk-button id="${renderResponse.getNamespace()}placeOrderBtn" class="tr-cart-btn-placeorder fm-cart-placeorder small ok-modal-btn">Place Order</sk-button>
		</div>
	</form>
</template>
<template id="${renderResponse.getNamespace()}template_finalMsg">
	<h2 class="tr-cart-label-successfully-ordered">Successfully placed the order. We will contact you.</h2>
	<button id="${renderResponse.getNamespace()}finalMsgOkBtn" class="tr-cart-btn-finok small ok-modal-btn">Ok, clear</button>
</template>


<% if (renderRequest.isUserInRole("administrator")) { %>
	<fm-cart-config-dialog
			id="${renderResponse.namespace}configDialog"
			ns="${renderResponse.namespace}"
			saveUrl="${saveSettingsURL.toString()}"
			settingsUrl="${getSettingsURL.toString()}"
			type="confirm" to-body
	>
		<h3>Settings</h3>
		<div>
			<% for (String settingName : settings) { %>
			<div>
				<label><%= settingName %></label>
				<sk-input type="text" name="<%= settingName %>" value="<%= (String) renderRequest.getAttribute(settingName) %>"></sk-input>
			</div>
			<% } %>
		</div>
		<template id="FmCartConfigDialogFooterTpl">
			<div>
				<sk-button action="cancel">Cancel</sk-button>
				<sk-button action="save" button-type="primary">Save</sk-button>
			</div>
		</template>
	</fm-cart-config-dialog>
	<button class="fm-btn-configure" id="${renderResponse.getNamespace()}configDialogOpenBtn"></button>
<% } %>

<cart-button ns="${renderResponse.getNamespace()}"
			 id="${renderResponse.getNamespace()}cartBtn"
			 sticky="${basketBtnSticky}"
			 sticky-offset="${basketBtnStickyOffset}"
			 settingsUrl="${getSettingsURL.toString()}"
			 placeOrderUrl="${placeOrderURL.toString()}"></cart-button>
<cart-dialog to-body id="${renderResponse.getNamespace()}cartDialog">
	<cart-list ns="${renderResponse.getNamespace()}" settingsUrl="${getSettingsURL.toString()}" placeOrderUrl="${placeOrderURL.toString()}"></cart-list>
</cart-dialog>

<script src="<%= ctxPath %>/fm-cart-portlet-bundle.js"></script>
<script>

    let ns = '${renderResponse.getNamespace()}';
    let settings = ${settingsJson};

	window.fmRegistry = window.fmRegistry || new Registry();

	fmRegistry.wire({
		EventBus,
		Renderer,
		CartService: { def: CartService, bootstrap: (target) => {
			target.bindEvents();
		}},
		CartListElement: { def: CartListElement, deps: {
			'cartService': CartService,
			'renderer': Renderer
		}, is: 'cart-list'},
		CartButtonElement: { def: CartButtonElement, deps: {
		    'eventBus': EventBus,
			'cartService': CartService
		}, is: 'cart-button'},
		FmCartConfigDialog: { def: FmCartConfigDialog, deps: {}, is: 'fm-cart-config-dialog'},
		CartDialog: { def: CartDialog, is: 'cart-dialog'}
	});

    let openButton = document.querySelector('#' + ns + 'configDialogOpenBtn');
    let configDialog = document.querySelector('#' + ns + 'configDialog');
    if (openButton !== null) {
		openButton.addEventListener('click', (event) => {
			configDialog.open();
		});
	}

    let cartButton = document.querySelector('#' + ns + 'cartBtn');
	// place cart to alternate position
	let alternativeSl = '${alternativeSelector}';
	let alternativeSlInsert = '${alternativeSelectorInsert}';
	if (alternativeSl !== '') {
		let el = document.querySelector(alternativeSl);
		if (el != null) {
			el.insertAdjacentHTML(alternativeSlInsert, cartButton.outerHTML);
			cartButton.parentElement.removeChild(cartButton);
			cartButton = el.querySelector('#' + ns + 'cartBtn');

		}
	}
    let cartDialog = document.querySelector('#' + ns + 'cartDialog');
    cartButton.addEventListener('click', (event) => {
		cartButton.showCart();
    });

</script>


