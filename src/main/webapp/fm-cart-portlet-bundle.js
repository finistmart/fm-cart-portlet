(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
  typeof define === 'function' && define.amd ? define(['exports'], factory) :
  (global = global || self, factory(global.window = global.window || {}));
}(this, (function (exports) { 'use strict';

  function _typeof(obj) {
    "@babel/helpers - typeof";

    if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
      _typeof = function (obj) {
        return typeof obj;
      };
    } else {
      _typeof = function (obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
      };
    }

    return _typeof(obj);
  }

  function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
    try {
      var info = gen[key](arg);
      var value = info.value;
    } catch (error) {
      reject(error);
      return;
    }

    if (info.done) {
      resolve(value);
    } else {
      Promise.resolve(value).then(_next, _throw);
    }
  }

  function _asyncToGenerator(fn) {
    return function () {
      var self = this,
          args = arguments;
      return new Promise(function (resolve, reject) {
        var gen = fn.apply(self, args);

        function _next(value) {
          asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
        }

        function _throw(err) {
          asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
        }

        _next(undefined);
      });
    };
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function");
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        writable: true,
        configurable: true
      }
    });
    if (superClass) _setPrototypeOf(subClass, superClass);
  }

  function _getPrototypeOf(o) {
    _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
      return o.__proto__ || Object.getPrototypeOf(o);
    };
    return _getPrototypeOf(o);
  }

  function _setPrototypeOf(o, p) {
    _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
      o.__proto__ = p;
      return o;
    };

    return _setPrototypeOf(o, p);
  }

  function _isNativeReflectConstruct() {
    if (typeof Reflect === "undefined" || !Reflect.construct) return false;
    if (Reflect.construct.sham) return false;
    if (typeof Proxy === "function") return true;

    try {
      Date.prototype.toString.call(Reflect.construct(Date, [], function () {}));
      return true;
    } catch (e) {
      return false;
    }
  }

  function _construct(Parent, args, Class) {
    if (_isNativeReflectConstruct()) {
      _construct = Reflect.construct;
    } else {
      _construct = function _construct(Parent, args, Class) {
        var a = [null];
        a.push.apply(a, args);
        var Constructor = Function.bind.apply(Parent, a);
        var instance = new Constructor();
        if (Class) _setPrototypeOf(instance, Class.prototype);
        return instance;
      };
    }

    return _construct.apply(null, arguments);
  }

  function _isNativeFunction(fn) {
    return Function.toString.call(fn).indexOf("[native code]") !== -1;
  }

  function _wrapNativeSuper(Class) {
    var _cache = typeof Map === "function" ? new Map() : undefined;

    _wrapNativeSuper = function _wrapNativeSuper(Class) {
      if (Class === null || !_isNativeFunction(Class)) return Class;

      if (typeof Class !== "function") {
        throw new TypeError("Super expression must either be null or a function");
      }

      if (typeof _cache !== "undefined") {
        if (_cache.has(Class)) return _cache.get(Class);

        _cache.set(Class, Wrapper);
      }

      function Wrapper() {
        return _construct(Class, arguments, _getPrototypeOf(this).constructor);
      }

      Wrapper.prototype = Object.create(Class.prototype, {
        constructor: {
          value: Wrapper,
          enumerable: false,
          writable: true,
          configurable: true
        }
      });
      return _setPrototypeOf(Wrapper, Class);
    };

    return _wrapNativeSuper(Class);
  }

  function _newArrowCheck(innerThis, boundThis) {
    if (innerThis !== boundThis) {
      throw new TypeError("Cannot instantiate an arrow function");
    }
  }

  function _assertThisInitialized(self) {
    if (self === void 0) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return self;
  }

  function _possibleConstructorReturn(self, call) {
    if (call && (typeof call === "object" || typeof call === "function")) {
      return call;
    }

    return _assertThisInitialized(self);
  }

  function _createSuper(Derived) {
    var hasNativeReflectConstruct = _isNativeReflectConstruct();

    return function _createSuperInternal() {
      var Super = _getPrototypeOf(Derived),
          result;

      if (hasNativeReflectConstruct) {
        var NewTarget = _getPrototypeOf(this).constructor;

        result = Reflect.construct(Super, arguments, NewTarget);
      } else {
        result = Super.apply(this, arguments);
      }

      return _possibleConstructorReturn(this, result);
    };
  }

  function _superPropBase(object, property) {
    while (!Object.prototype.hasOwnProperty.call(object, property)) {
      object = _getPrototypeOf(object);
      if (object === null) break;
    }

    return object;
  }

  function _get(target, property, receiver) {
    if (typeof Reflect !== "undefined" && Reflect.get) {
      _get = Reflect.get;
    } else {
      _get = function _get(target, property, receiver) {
        var base = _superPropBase(target, property);

        if (!base) return;
        var desc = Object.getOwnPropertyDescriptor(base, property);

        if (desc.get) {
          return desc.get.call(receiver);
        }

        return desc.value;
      };
    }

    return _get(target, property, receiver || target);
  }

  function _slicedToArray(arr, i) {
    return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest();
  }

  function _arrayWithHoles(arr) {
    if (Array.isArray(arr)) return arr;
  }

  function _iterableToArrayLimit(arr, i) {
    if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return;
    var _arr = [];
    var _n = true;
    var _d = false;
    var _e = undefined;

    try {
      for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
        _arr.push(_s.value);

        if (i && _arr.length === i) break;
      }
    } catch (err) {
      _d = true;
      _e = err;
    } finally {
      try {
        if (!_n && _i["return"] != null) _i["return"]();
      } finally {
        if (_d) throw _e;
      }
    }

    return _arr;
  }

  function _unsupportedIterableToArray(o, minLen) {
    if (!o) return;
    if (typeof o === "string") return _arrayLikeToArray(o, minLen);
    var n = Object.prototype.toString.call(o).slice(8, -1);
    if (n === "Object" && o.constructor) n = o.constructor.name;
    if (n === "Map" || n === "Set") return Array.from(o);
    if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
  }

  function _arrayLikeToArray(arr, len) {
    if (len == null || len > arr.length) len = arr.length;

    for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];

    return arr2;
  }

  function _nonIterableRest() {
    throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
  }

  function _createForOfIteratorHelper(o, allowArrayLike) {
    var it;

    if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) {
      if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") {
        if (it) o = it;
        var i = 0;

        var F = function () {};

        return {
          s: F,
          n: function () {
            if (i >= o.length) return {
              done: true
            };
            return {
              done: false,
              value: o[i++]
            };
          },
          e: function (e) {
            throw e;
          },
          f: F
        };
      }

      throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
    }

    var normalCompletion = true,
        didErr = false,
        err;
    return {
      s: function () {
        it = o[Symbol.iterator]();
      },
      n: function () {
        var step = it.next();
        normalCompletion = step.done;
        return step;
      },
      e: function (e) {
        didErr = true;
        err = e;
      },
      f: function () {
        try {
          if (!normalCompletion && it.return != null) it.return();
        } finally {
          if (didErr) throw err;
        }
      }
    };
  }

  var HgTemplateEngine = /*#__PURE__*/function () {
    function HgTemplateEngine(handlebars) {
      _classCallCheck(this, HgTemplateEngine);

      this.handlebars = handlebars;
    }

    _createClass(HgTemplateEngine, [{
      key: "renderString",
      value: function renderString(tplString, data) {
        var templateFunc = this.handlebars.compile(tplString);
        var result = templateFunc(data);
        return result;
      }
    }, {
      key: "render",
      value: function render(tpl, data) {
        if (_typeof(tpl) === 'object') {
          var wrapper = document.createElement('div');
          wrapper.appendChild(tpl);
          var templateString = wrapper.outerHTML.toString();
          var rendered = this.renderString(templateString, data);
          var wrapper2 = document.createElement('div');
          wrapper2.insertAdjacentHTML('beforeend', rendered);
          return wrapper2.firstElementChild.innerHTML;
        } else {
          return this.renderString(tpl, data);
        }
      }
    }]);

    return HgTemplateEngine;
  }();

  var BaseTemplateEngine = /*#__PURE__*/function () {
    function BaseTemplateEngine() {
      _classCallCheck(this, BaseTemplateEngine);
    }

    _createClass(BaseTemplateEngine, [{
      key: "renderMustacheVars",
      value: function renderMustacheVars(el, map) {
        var wrapper = document.createElement('div');
        wrapper.appendChild(el);
        var template = wrapper.outerHTML.toString();

        for (var _i = 0, _Object$keys = Object.keys(map); _i < _Object$keys.length; _i++) {
          var key = _Object$keys[_i];
          template = template.replace(new RegExp('{{ ' + key + ' }}', 'g'), map[key]);
        }

        var wrapper2 = document.createElement('div');
        wrapper2.insertAdjacentHTML('beforeend', template);
        return wrapper2.firstElementChild.innerHTML;
      }
    }, {
      key: "renderString",
      value: function renderString(tplString, data) {
        var resultString = (' ' + tplString).slice(1);

        for (var _i2 = 0, _Object$keys2 = Object.keys(data); _i2 < _Object$keys2.length; _i2++) {
          var key = _Object$keys2[_i2];
          resultString = resultString.replace(new RegExp('{{ ' + key + ' }}', 'g'), data[key]);
        }

        return resultString;
      }
    }, {
      key: "render",
      value: function render(tpl, data) {
        if (_typeof(tpl) === 'object') {
          return this.renderMustacheVars(tpl, data);
        } else {
          return this.renderString(tpl, data);
        }
      }
    }]);

    return BaseTemplateEngine;
  }();

  var RD_SHARED_ATTRS = {
    'rd-cache': 'cacheTemplates',
    'rd-cache-global': 'allowGlobalTemplates',
    'rd-var-render': 'variableRender',
    'rd-tpl-fmt': 'tplFmt',
    'basepath': 'basePath',
    'theme': 'theme'
  };
  var Renderer = /*#__PURE__*/function () {
    //templates = {};
    function Renderer() {
      _classCallCheck(this, Renderer);

      this.templates = {};
      this.templateLoad = {};
      this.translations = {};
      this.cacheTemplates = true;
      this.allowGlobalTemplates = true;
      this.variableRender = true;

      if (window['$'] !== undefined || window['jQuery'] !== undefined) {
        this.jquery = $ || jQuery.noConflict();
      }
    }

    _createClass(Renderer, [{
      key: "confFromEl",
      value: function confFromEl(el) {
        for (var _i = 0, _Object$keys = Object.keys(RD_SHARED_ATTRS); _i < _Object$keys.length; _i++) {
          var attrName = _Object$keys[_i];
          var value = el.getAttribute(attrName);

          if (value !== null && value !== undefined) {
            try {
              this[RD_SHARED_ATTRS[attrName]] = JSON.parse(value);
            } catch (e) {
              this[RD_SHARED_ATTRS[attrName]] = value;
            }
          }
        }
      }
    }, {
      key: "queryTemplate",
      value: function queryTemplate(sl) {
        if (this.templates[sl] !== null && this.templates[sl] !== undefined) {
          return this.templates[sl];
        } else {
          var template = document.querySelector(sl);

          if (template !== null && template !== undefined) {
            this.templates[sl] = template;
          } else {
            console.warn("Template with selector ".concat(sl, " not found"));
          }

          return this.templates[sl];
        }
      }
    }, {
      key: "loadTemplate",
      value: function loadTemplate(path) {
        var _this = this;

        return new Promise(function (resolve, reject) {
          _newArrowCheck(this, _this);

          fetch(path).then(function (response) {
            var _this2 = this;

            response.text().then(function (text) {
              _newArrowCheck(this, _this2);

              resolve(text);
            }.bind(this));
          });
        }.bind(this));
      }
    }, {
      key: "findTemplateEl",
      value: function findTemplateEl(id, hostEl) {
        var el = null;

        if (this.cacheTemplates) {
          if (hostEl) {
            el = hostEl.querySelector('#' + id);

            if (!el) {
              el = hostEl.querySelector(id);
            }
          }

          if (this.allowGlobalTemplates) {
            if (!el) {
              el = document.getElementById(id);

              if (!el) {
                el = document.querySelector(id);
              }
            }
          }
        }

        return el;
      }
    }, {
      key: "mountTemplate",
      value: function mountTemplate(path, id, hostEl, preRenData) {
        var _this3 = this;

        var tplHash = path + JSON.stringify(preRenData);

        if (!this.templateLoad[tplHash]) {
          this.templateLoad[tplHash] = new Promise(function (resolve, reject) {
            var _this4 = this;

            _newArrowCheck(this, _this3);

            var el = this.findTemplateEl(id, hostEl);

            if (el !== null) {
              var elInstance = document.importNode(el, true); // can be mustache or native template inlined and overriden

              if (this.variableRender && this.templateEngine && preRenData) {
                elInstance.innerHTML = this.templateEngine.render(elInstance.innerHTML, preRenData);
              }

              resolve(elInstance);
            } else {
              this.loadTemplate(path).then(function (body) {
                _newArrowCheck(this, _this4);

                var template = this.findTemplateEl(id, hostEl);

                if (!template) {
                  if (this.tplFmt === 'handlebars') {
                    template = this.createEl('script');
                    template.setAttribute('type', 'text/x-handlebars-template');
                  } else {
                    template = this.createEl('template');
                  }
                }

                template.setAttribute('id', id);
                template.innerHTML = body;

                if (this.cacheTemplates) {
                  el = this.findTemplateEl(id, hostEl);

                  if (this.allowGlobalTemplates) {
                    document.body.appendChild(template);
                  } else {
                    hostEl.appendChild(template);
                  }

                  el = this.findTemplateEl(id, hostEl);
                }

                if (this.variableRender && this.templateEngine && preRenData) {
                  var contents = this.templateEngine.render(body, preRenData);
                  var templateCopy = document.importNode(template, true);
                  templateCopy.innerHTML = contents;
                  resolve(templateCopy);
                } else {
                  resolve(el);
                }
              }.bind(this));
            }
          }.bind(this));
        }

        return this.templateLoad[tplHash] || Promise.reject('Template load error', tplHash);
      }
    }, {
      key: "prepareTemplate",
      value: function prepareTemplate(tpl) {
        if (this.tplFmt && this.tplFmt === 'handlebars') {
          var el = this.jquery(tpl.innerHTML);
          var html = '';

          if (el.length > 0) {
            html = this.jquery().prop ? this.jquery(tpl.innerHTML).prop('outerHTML') : this.jquery(tpl.innerHTML)[0].outerHTML;
          }

          var wrapper = this.createEl('div');
          this.jquery(wrapper).append(html);
          return wrapper.firstElementChild;
        } else {
          return document.importNode(tpl.content, true);
        }
      }
    }, {
      key: "renderTemplate",
      value: function renderTemplate(sl, dataMap) {
        var template = this.queryTemplate(sl);
        var fragment = document.importNode(template.content, true);

        for (var _i2 = 0, _Object$keys2 = Object.keys(dataMap); _i2 < _Object$keys2.length; _i2++) {
          var _sl = _Object$keys2[_i2];
          var entry = dataMap[_sl];
          var el = fragment.querySelector(_sl);

          if (el !== null) {
            if (entry.type === 'attr') {
              el.setAttribute(entry.attr, entry.value);
            } else {
              el.textContent = entry.value;
            }
          } else {
            console.warn("Element with selector ".concat(_sl, " not found in DOM"));
          }
        }

        return fragment;
      }
    }, {
      key: "renderMustacheVars",
      value: function renderMustacheVars(el, map) {
        var wrapper = this.createEl('div');
        wrapper.appendChild(el);
        var template = wrapper.outerHTML.toString(); // :TODO detect if document-fragment or element is passed

        for (var _i3 = 0, _Object$keys3 = Object.keys(map); _i3 < _Object$keys3.length; _i3++) {
          var key = _Object$keys3[_i3];
          template = template.replace(new RegExp('{{ ' + key + ' }}', 'g'), map[key]);
        }

        var wrapper2 = this.createEl('div');
        wrapper2.insertAdjacentHTML('beforeend', template);
        return wrapper2.firstElementChild.innerHTML;
      }
    }, {
      key: "createEl",
      value: function createEl(tagName, options) {
        return document.createElement(tagName, options);
      }
    }, {
      key: "renderWithTr",
      value: function renderWithTr(trJson, targetEl) {
        var _this5 = this;

        var fakeRoot = this.createEl('div');
        fakeRoot.appendChild(targetEl);
        fakeRoot.querySelectorAll('[tr]').forEach(function (el) {
          _newArrowCheck(this, _this5);

          var tr = el.getAttribute('tr');

          if (tr === 'contents') {
            var trValue = trJson[el.innerHTML];

            if (trValue !== undefined) {
              el.innerHTML = trValue;
            }
          } else if (tr.startsWith('attr')) {
            var trInfo = tr.split(':');
            var _trValue = trJson[trInfo[2]];

            if (_trValue !== undefined) {
              el.setAttribute(trInfo[1], _trValue);
            }
          }
        }.bind(this));
        return fakeRoot.firstChild.cloneNode(true);
      }
    }, {
      key: "localizeEl",
      value: function localizeEl(targetEl, locale) {
        var _this6 = this;

        var self = this;
        return new Promise(function (resolve, reject) {
          _newArrowCheck(this, _this6);

          if (!this.translations[locale]) {
            fetch(self.basePath + '/theme/' + this.theme + '/i18n/' + 'gridy-i18n-' + locale + '.json').then(function (response) {
              var _this7 = this;

              response.json().then(function (json) {
                _newArrowCheck(this, _this7);

                self.translations[locale] = json;
                resolve(self.renderWithTr(self.translations[locale], targetEl));
              }.bind(this));
            });
          } else {
            resolve(self.renderWithTr(this.translations[locale], targetEl));
          }
        }.bind(this));
      }
    }, {
      key: "templateEngine",
      get: function get() {
        if (!this._templateEngine) {
          if (this.variableRender === 'handlebars') {
            if (this.Handlebars !== undefined) {
              this._templateEngine = new HgTemplateEngine(this.Handlebars);
            } else {
              if (window && window['Handlebars']) {
                this._templateEngine = new HgTemplateEngine(window['Handlebars']);
              } else {
                console.error('no handlebars found in window or property');
              }
            }
          } else {
            if (typeof window[this.variableRender] === 'function') {
              window[this.variableRender].call(this);
            } else {
              this._templateEngine = new BaseTemplateEngine();
            }
          }
        }

        return this._templateEngine;
      }
    }], [{
      key: "hasRenderAttrs",
      value: function hasRenderAttrs(el) {
        for (var _i4 = 0, _Object$keys4 = Object.keys(RD_SHARED_ATTRS); _i4 < _Object$keys4.length; _i4++) {
          var attrName = _Object$keys4[_i4];
          var value = el.getAttribute(attrName);

          if (value !== null && value !== undefined) {
            return true;
          }
        }

        return false;
      }
    }, {
      key: "configureForElement",
      value: function configureForElement(el) {
        if (!el.renderer) {
          el.renderer = new Renderer();
          el.renderer.confFromEl(el);
        } else {
          // if one of renderer options specified element needs own extended renderer
          if (Renderer.hasRenderAttrs(el)) {
            var rendererCopy = el.renderer;
            el.renderer = new Renderer();

            for (var _i5 = 0, _Object$keys5 = Object.keys(RD_SHARED_ATTRS); _i5 < _Object$keys5.length; _i5++) {
              var attrName = _Object$keys5[_i5];

              if (rendererCopy[RD_SHARED_ATTRS[attrName]] !== null && rendererCopy[RD_SHARED_ATTRS[attrName]] !== undefined) {
                el.renderer[RD_SHARED_ATTRS[attrName]] = rendererCopy[RD_SHARED_ATTRS[attrName]];
              }
            }

            el.renderer.confFromEl(el);
          }
        }
      }
    }]);

    return Renderer;
  }();

  var Registry = /*#__PURE__*/function () {
    function Registry() {
      _classCallCheck(this, Registry);

      this.registry = {};
    }

    _createClass(Registry, [{
      key: "getDep",
      value: function getDep(depName) {
        return this.registry[depName];
      }
      /**
       * If you use shared registry beaware deps with same name are not overriding by default, use ov param for that
       * example:
       *
       * window.fmRegistry = window.fmRegistry || new Registry();
         fmRegistry.wire({
          Renderer,
          catalogTr: { val: { foo: 'bar' }},
          FmProductGrid: { def: FmProductGrid, deps: {
                  'renderer': Renderer
              }, is: 'fm-product-grid'}
          });
        * @param definitionName
       * @param map
       * @returns {*}
       */

    }, {
      key: "wireDep",
      value: function wireDep(definitionName, map) {
        if (!this.registry[definitionName] || map[definitionName].ov) {
          if (map[definitionName] && map[definitionName].def) {
            // definition
            if (map[definitionName].is) {
              this.registry[definitionName] = map[definitionName].def;
            } else {
              this.registry[definitionName] = new map[definitionName].def();
            }
          } else if (map[definitionName] && map[definitionName].val !== undefined && map[definitionName].val !== null) {
            this.registry[definitionName] = map[definitionName].val;
          } else if (map[definitionName] && map[definitionName].f) {
            // explicit factory
            if (typeof new map[definitionName].f() === 'function') {
              this.registry[definitionName] = map[definitionName].f.call(map[definitionName].f);
            } else {
              console.error('factory must be a function');
            }
          } else {
            // function or constructor
            if (typeof map[definitionName] === 'function') {
              // guess constructor
              try {
                this.registry[definitionName] = new map[definitionName]();
              } catch (_unused) {
                // if not constructor then factory
                console.warn('constructor not provided for ' + definitionName + ', guess this is a factory');
                this.registry[definitionName] = map[definitionName].call(map[definitionName]);
              }
            } else {
              // just assign the link
              this.registry[definitionName] = map[definitionName];
            }
          }
        }

        return this.registry[definitionName];
      }
    }, {
      key: "wire",
      value: function wire(map) {
        for (var _i = 0, _Object$keys = Object.keys(map); _i < _Object$keys.length; _i++) {
          var definitionName = _Object$keys[_i];
          this.wireDep(definitionName, map);
          var definition = map[definitionName];

          if (definition.deps) {
            var depDefs = Object.keys(definition.deps);

            for (var _i2 = 0, _depDefs = depDefs; _i2 < _depDefs.length; _i2++) {
              var propName = _depDefs[_i2];
              var propDef = definition.deps[propName];
              var defName = typeof propDef === 'string' ? propDef : propDef.name;
              var dependency = this.registry[defName];

              if (!dependency) {
                dependency = this.registry[propDef.name] = this.wireDep(defName, map);
              }

              if (definition.is) {
                Object.defineProperty(this.registry[definitionName].prototype, propName, {
                  value: dependency,
                  writable: true
                });
              } else {
                Object.defineProperty(this.registry[definitionName], propName, {
                  value: dependency,
                  writable: true
                });
              }
            }
          }

          if (definition.bootstrap) {
            definition.bootstrap(this.registry[definitionName]);
          }

          if (definition.is) {
            if (!window.customElements.get(definition.is)) {
              window.customElements.define(definition.is, this.registry[definitionName]);
            }
          }
        }
      }
    }]);

    return Registry;
  }();

  var EventBus = /*#__PURE__*/function () {
    function EventBus() {
      _classCallCheck(this, EventBus);
    }

    _createClass(EventBus, [{
      key: "addEventListener",
      value: function addEventListener(type, listener) {
        return document.addEventListener(type, listener);
      }
    }]);

    return EventBus;
  }();

  var CartService = /*#__PURE__*/function () {
    function CartService() {
      _classCallCheck(this, CartService);

      console.log('cart service element ctor');
      var storedItems = JSON.parse(sessionStorage.getItem('fm-cart-items'));
      this.items = storedItems || {};
      this.delivery = {};
      this.callbacks = [];
    }

    _createClass(CartService, [{
      key: "addItem",
      value: function addItem(event) {
        console.log('addItem', event);
        var itemToAdd = event.detail.item;
        var qty = itemToAdd.qty || 1;
        var keys = Object.keys(this.items);
        var itemExists = false;

        for (var _i = 0, _keys = keys; _i < _keys.length; _i++) {
          var itemKey = _keys[_i];
          var item = this.items[itemKey];

          if (item.title == itemToAdd.title) {
            this.items[itemKey].qty += qty;
            itemExists = true;
          }
        }

        if (!itemExists) {
          itemToAdd['qty'] = qty;
          this.items[Object.keys(this.items).length] = itemToAdd;
        }

        this.applyCallbacks();
        sessionStorage.setItem('fm-cart-items', JSON.stringify(this.items));
      }
    }, {
      key: "applyCallbacks",
      value: function applyCallbacks() {
        var _this = this;

        this.callbacks.forEach(function (callback, i) {
          _newArrowCheck(this, _this);

          callback();
        }.bind(this));
      }
    }, {
      key: "bindUpdate",
      value: function bindUpdate(callback) {
        this.callbacks.push(callback);
      }
    }, {
      key: "placeOrder",
      value: function placeOrder(url) {
        var _this2 = this;

        return new Promise(function (resolve, reject) {
          var _this3 = this;

          _newArrowCheck(this, _this2);

          var xhr = new XMLHttpRequest();
          xhr.open("POST", url);
          xhr.setRequestHeader("Content-Type", 'application/x-www-form-urlencoded');

          xhr.onreadystatechange = function (event) {
            _newArrowCheck(this, _this3);

            if (event.target.readyState == XMLHttpRequest.DONE) {
              if (event.target.status == 200) {
                this.items = {};
                this.applyCallbacks();
                resolve();
              } else {
                reject();
              }
            }
          }.bind(this);

          xhr.send('delivery=' + JSON.stringify(this.delivery) + '&items=' + JSON.stringify(this.items));
        }.bind(this));
      }
    }, {
      key: "clear",
      value: function clear() {
        this.items = [];
        sessionStorage.setItem('fm-cart-items', JSON.stringify(this.items));
      }
    }, {
      key: "removeItem",
      value: function removeItem(itemToDel) {
        console.log('removeItem', itemToDel);
        var keys = Object.keys(this.items);

        for (var _i2 = 0, _keys2 = keys; _i2 < _keys2.length; _i2++) {
          var itemKey = _keys2[_i2];
          var item = this.items[itemKey];

          if (item.title == itemToDel.title) {
            delete this.items[itemKey];
          }
        }

        this.applyCallbacks();
        sessionStorage.setItem('fm-cart-items', JSON.stringify(this.items));
      }
    }, {
      key: "bindEvents",
      value: function bindEvents(ns) {
        if (ns == null || ns == undefined) {
          ns = ''; //bind as global
        }

        document.addEventListener(ns + 'cart:additem', this.addItem.bind(this));
        document.addEventListener(ns + 'cart:removeitem', this.removeItem.bind(this));
        document.addEventListener(ns + 'cart:clear', this.clear.bind(this));
      }
    }, {
      key: "count",
      get: function get() {
        return Object.keys(this.items).length;
      }
    }]);

    return CartService;
  }();

  function loadFromUrl(url) {
    var _this = this;

    return new Promise(function (resolve, reject) {
      var _this2 = this;

      _newArrowCheck(this, _this);

      var xhr = new XMLHttpRequest();
      xhr.open("GET", url);
      xhr.setRequestHeader("Content-Type", 'application/json');

      xhr.onreadystatechange = function (event) {
        _newArrowCheck(this, _this2);

        if (event.target.readyState == XMLHttpRequest.DONE) {
          if (event.target.status == 200) {
            var settings = JSON.parse(event.target.response);
            resolve(settings);
          } else {
            reject(event);
          }
        }
      }.bind(this);

      xhr.send();
    }.bind(this));
  }

  var CartListElement = /*#__PURE__*/function (_HTMLElement) {
    _inherits(CartListElement, _HTMLElement);

    var _super = _createSuper(CartListElement);

    //renderer: Renderer;
    //cartService: CartService;
    function CartListElement() {
      var _this;

      _classCallCheck(this, CartListElement);

      _this = _super.call(this);
      console.log('cart list element ctor');
      return _this;
    }

    _createClass(CartListElement, [{
      key: "refresh",
      value: function refresh() {
        if (this.settings) {
          this.renderTemplate(this.settings);
        }
      }
    }, {
      key: "onItemSelected",
      value: function onItemSelected() {
        document.dispatchEvent(new CustomEvent('cart-list:showdelivery'));
      }
    }, {
      key: "connectedCallback",
      value: function connectedCallback() {
        this.loadSettings().then(function (settings) {
          var _this2 = this;

          this.settings = settings;
          this.renderTemplate(settings);

          if (!this.eventsBinded) {
            if (!window.showDeliveryHandler) {
              window.showDeliveryHandler = this.renderDeliveryForm.bind(this);
              document.addEventListener('cart-list:showdelivery', window.showDeliveryHandler);
            }

            if (!window.itemSelectedHandler) {
              window.itemSelectedHandler = this.onItemSelected.bind(this);
              document.addEventListener('cart-list:itemsselected', window.itemSelectedHandler);
            }

            document.dispatchEvent(new CustomEvent('cart-list:rendered'));
            this.addEventListener('cart-list:refresh', function (event) {
              _newArrowCheck(this, _this2);

              this.refresh();
            }.bind(this));
            this.eventsBinded = true;
          }
        }.bind(this));
      }
    }, {
      key: "disconnectedCallback",
      value: function disconnectedCallback() {
        if (window.showDeliveryHandler) {
          document.removeEventListener('cart-list:showdelivery', this.showDeliveryHandler);
          delete window.showDeliveryHandler;
        }

        if (window.itemSelectedHandler) {
          document.removeEventListener('cart-list:itemsselected', this.itemSelectedHandler);
          delete window.itemSelectedHandler;
        }

        if (window.placeOrderHandler) {
          delete window.placeOrderHandler;
        }
      }
    }, {
      key: "loadSettings",
      value: function loadSettings() {
        return loadFromUrl(this.settingsUrl);
      }
    }, {
      key: "translate",
      value: function translate(settings, el) {
        var _this3 = this;

        el = el ? el : this;
        var trElements = el.querySelectorAll('[class^=tr]');

        if (trElements !== null) {
          trElements.forEach(function (el, index) {
            _newArrowCheck(this, _this3);

            var _iterator = _createForOfIteratorHelper(el.classList),
                _step;

            try {
              for (_iterator.s(); !(_step = _iterator.n()).done;) {
                var className = _step.value;

                if (className.startsWith('tr-')) {
                  var normedClassName = className.replace(/-/g, '.');
                  var tr = this.settings.l11n[normedClassName];

                  if (tr) {
                    el.textContent = tr;
                  }
                }
              }
            } catch (err) {
              _iterator.e(err);
            } finally {
              _iterator.f();
            }
          }.bind(this));
        }
      }
    }, {
      key: "renderTemplate",
      value: function renderTemplate(settings) {
        console.log('cart list settings', settings);
        this.template = document.getElementById(this.ns + 'template-cart-list');
        this.itemTemplate = document.getElementById(this.ns + 'template-cart-item');
        this.templateEmpty = document.getElementById(this.ns + 'template-cart-list-empty');
        this.innerHTML = '';
        var itemKeys = Object.keys(this.cartService.items);

        if (itemKeys.length > 0) {
          this.appendChild(document.importNode(this.template.content, true));
          var listContainer = this.querySelector('.fm-cart-container');

          if (listContainer !== null) {
            for (var _i = 0, _Object$keys = Object.keys(this.cartService.items); _i < _Object$keys.length; _i++) {
              var no = _Object$keys[_i];
              var cartItem = this.cartService.items[no];
              var rowNum = parseInt(no) + 1;
              var rowNode = this.renderer.renderTemplate('#' + this.ns + 'template-cart-item', {
                '.fm-cart-no': {
                  value: rowNum
                },
                '.fm-cart-title': {
                  value: cartItem.title
                },
                '.fm-cart-price': {
                  value: cartItem.price
                },
                '.fm-cart-qty': {
                  value: cartItem.qty
                }
              });
              rowNode.querySelector('.action-remove').addEventListener('click', this.removeItem.bind(this, cartItem));
              listContainer.appendChild(rowNode);
            }
          }

          var buyBtn = this.querySelector('#' + this.ns + 'cartListBuyBtn');

          if (buyBtn !== null) {
            buyBtn.addEventListener('click', this.renderDeliveryForm.bind(this));
          }
        } else {
          this.appendChild(document.importNode(this.templateEmpty.content, true));
        }

        this.translate(settings);
      }
    }, {
      key: "valueByName",
      value: function valueByName(name) {
        var el = document.querySelector('[name=' + name + ']');

        if (el != null) {
          return el.value;
        } else {
          return null;
        }
      }
    }, {
      key: "processDeliveryForm",
      value: function processDeliveryForm() {
        var _this4 = this;

        if (!this.proceddDelivery) {
          this.proceddDelivery = new Promise(function (resolve, reject) {
            _newArrowCheck(this, _this4);

            var form = document.querySelector('#' + this.ns + 'deliveryForm');

            if (form !== null && form.checkValidity()) {
              var data = {
                email: this.valueByName('email'),
                state: this.valueByName('state'),
                city: this.valueByName('city'),
                zipcode: this.valueByName('zipcode'),
                address1: this.valueByName('address1'),
                address2: this.valueByName('address2'),
                fullName: this.valueByName('fullName'),
                tel: this.valueByName('tel')
              };
              resolve(data);
            } else {
              reject();
            }
          }.bind(this));
        }

        return this.proceddDelivery;
      }
    }, {
      key: "renderFinalMsg",
      value: function renderFinalMsg() {
        var _this5 = this;

        var templateFinalMsg = document.importNode(document.getElementById(this.ns + 'template_finalMsg'), true); //let body = this.cartDialog.impl.dialog.querySelector('.ant-modal-body');

        this.innerHTML = '';
        this.appendChild(templateFinalMsg.content);
        this.translate(this.settings, this.cartDialog);
        var okBtn = document.getElementById(this.ns + 'finalMsgOkBtn');
        okBtn.addEventListener('click', function () {
          _newArrowCheck(this, _this5);

          document.dispatchEvent(new CustomEvent('cart:clear'));
          this.renderTemplate();
          this.cartDialog.close();
        }.bind(this));
      }
    }, {
      key: "onPlaceOrderClick",
      value: function onPlaceOrderClick(event) {
        var _this6 = this;

        event.preventDefault();
        event.stopPropagation();
        this.processDeliveryForm().then(function (data) {
          var _this7 = this;

          _newArrowCheck(this, _this6);

          this.cartService.delivery = data;
          this.placeOrder(this.placeOrderUrl).then(function () {
            _newArrowCheck(this, _this7);

            this.renderFinalMsg();
          }.bind(this), function () {
            _newArrowCheck(this, _this7);

            console.error('error placing order');
          }.bind(this));
        }.bind(this), function () {
          _newArrowCheck(this, _this6);

          console.error('error validating order');
        }.bind(this));
      }
    }, {
      key: "renderDeliveryForm",
      value: function renderDeliveryForm() {
        var _this8 = this;

        this.templateDelivery = document.getElementById(this.ns + 'template-delivery');
        this.innerHTML = '';
        this.appendChild(document.importNode(this.templateDelivery.content, true));
        this.translate(this.settings);
        this.cartDialog = document.getElementById(this.ns + 'cartDialog');
        var placeBtn = this.querySelector('#' + this.ns + 'placeOrderBtn');

        if (placeBtn !== null) {
          if (!window.placeOrderHandler) {
            window.placeOrderHandler = this.onPlaceOrderClick.bind(this);
            placeBtn.onclick = window.placeOrderHandler;
          }
        }

        var cancelBtn = this.querySelector('.cancel-modal-btn');

        if (cancelBtn !== null) {
          if (this.cancelBinded) {
            cancelBtn.addEventListener('click', function (event) {
              _newArrowCheck(this, _this8);

              event.preventDefault();
              event.stopPropagation();
              this.cartDialog.close();
            }.bind(this));
            this.cancelBinded = true;
          }
        }
      }
    }, {
      key: "placeOrder",
      value: function placeOrder() {
        return this.cartService.placeOrder(this.placeOrderUrl);
      }
    }, {
      key: "removeItem",
      value: function removeItem(item) {
        this.cartService.removeItem(item);
        this.renderTemplate();
      }
    }, {
      key: "ns",
      get: function get() {
        return this.getAttribute('ns') || '';
      }
    }, {
      key: "placeOrderUrl",
      get: function get() {
        return this.getAttribute('placeOrderUrl') || '';
      }
    }, {
      key: "settingsUrl",
      get: function get() {
        return this.getAttribute('settingsUrl') || '';
      }
    }]);

    return CartListElement;
  }( /*#__PURE__*/_wrapNativeSuper(HTMLElement));

  var HgTemplateEngine$1 = /*#__PURE__*/function () {
    function HgTemplateEngine(handlebars) {
      _classCallCheck(this, HgTemplateEngine);

      this.handlebars = handlebars;
    }

    _createClass(HgTemplateEngine, [{
      key: "renderString",
      value: function renderString(tplString, data) {
        var templateFunc = this.handlebars.compile(tplString);
        var result = templateFunc(data);
        return result;
      }
    }, {
      key: "render",
      value: function render(tpl, data) {
        if (_typeof(tpl) === 'object') {
          var wrapper = document.createElement('div');
          wrapper.appendChild(tpl);
          var templateString = wrapper.outerHTML.toString();
          var rendered = this.renderString(templateString, data);
          var wrapper2 = document.createElement('div');
          wrapper2.insertAdjacentHTML('beforeend', rendered);
          return wrapper2.firstElementChild.innerHTML;
        } else {
          return this.renderString(tpl, data);
        }
      }
    }]);

    return HgTemplateEngine;
  }();

  var BaseTemplateEngine$1 = /*#__PURE__*/function () {
    function BaseTemplateEngine() {
      _classCallCheck(this, BaseTemplateEngine);
    }

    _createClass(BaseTemplateEngine, [{
      key: "renderMustacheVars",
      value: function renderMustacheVars(el, map) {
        var wrapper = document.createElement('div');
        wrapper.appendChild(el);
        var template = wrapper.outerHTML.toString();

        for (var _i = 0, _Object$keys = Object.keys(map); _i < _Object$keys.length; _i++) {
          var key = _Object$keys[_i];
          template = template.replace(new RegExp('{{ ' + key + ' }}', 'g'), map[key]);
        }

        var wrapper2 = document.createElement('div');
        wrapper2.insertAdjacentHTML('beforeend', template);
        return wrapper2.firstElementChild.innerHTML;
      }
    }, {
      key: "renderString",
      value: function renderString(tplString, data) {
        var resultString = (' ' + tplString).slice(1);

        for (var _i2 = 0, _Object$keys2 = Object.keys(data); _i2 < _Object$keys2.length; _i2++) {
          var key = _Object$keys2[_i2];
          resultString = resultString.replace(new RegExp('{{ ' + key + ' }}', 'g'), data[key]);
        }

        return resultString;
      }
    }, {
      key: "render",
      value: function render(tpl, data) {
        if (_typeof(tpl) === 'object') {
          return this.renderMustacheVars(tpl, data);
        } else {
          return this.renderString(tpl, data);
        }
      }
    }]);

    return BaseTemplateEngine;
  }();

  var RD_SHARED_ATTRS$1 = {
    'rd-cache': 'cacheTemplates',
    'rd-cache-global': 'allowGlobalTemplates',
    'rd-var-render': 'variableRender',
    'rd-tpl-fmt': 'tplFmt',
    'basepath': 'basePath',
    'theme': 'theme'
  };
  var Renderer$1 = /*#__PURE__*/function () {
    //templates = {};
    function Renderer() {
      _classCallCheck(this, Renderer);

      this.templates = {};
      this.templateLoad = {};
      this.translations = {};
      this.cacheTemplates = true;
      this.allowGlobalTemplates = true;
      this.variableRender = true;

      if (window['$'] !== undefined || window['jQuery'] !== undefined) {
        this.jquery = $ || jQuery.noConflict();
      }
    }

    _createClass(Renderer, [{
      key: "confFromEl",
      value: function confFromEl(el) {
        for (var _i = 0, _Object$keys = Object.keys(RD_SHARED_ATTRS$1); _i < _Object$keys.length; _i++) {
          var attrName = _Object$keys[_i];
          var value = el.getAttribute(attrName);

          if (value !== null && value !== undefined) {
            try {
              this[RD_SHARED_ATTRS$1[attrName]] = JSON.parse(value);
            } catch (e) {
              this[RD_SHARED_ATTRS$1[attrName]] = value;
            }
          }
        }
      }
    }, {
      key: "queryTemplate",
      value: function queryTemplate(sl) {
        if (this.templates[sl] !== null && this.templates[sl] !== undefined) {
          return this.templates[sl];
        } else {
          var template = document.querySelector(sl);

          if (template !== null && template !== undefined) {
            this.templates[sl] = template;
          } else {
            console.warn("Template with selector ".concat(sl, " not found"));
          }

          return this.templates[sl];
        }
      }
    }, {
      key: "loadTemplate",
      value: function loadTemplate(path) {
        var _this = this;

        return new Promise(function (resolve, reject) {
          _newArrowCheck(this, _this);

          fetch(path).then(function (response) {
            var _this2 = this;

            response.text().then(function (text) {
              _newArrowCheck(this, _this2);

              resolve(text);
            }.bind(this));
          });
        }.bind(this));
      }
    }, {
      key: "findTemplateEl",
      value: function findTemplateEl(id, hostEl) {
        var el = null;

        if (this.cacheTemplates) {
          if (hostEl) {
            el = hostEl.querySelector('#' + id);

            if (!el) {
              el = hostEl.querySelector(id);
            }
          }

          if (this.allowGlobalTemplates) {
            if (!el) {
              el = document.getElementById(id);

              if (!el) {
                el = document.querySelector(id);
              }
            }
          }
        }

        return el;
      }
    }, {
      key: "genElPathString",
      value: function genElPathString(el) {
        var path = el.nodeName.split("-").join('__');
        var parent = el.parentNode;

        while (parent && parent.tagName) {
          path = parent.nodeName.split("-").join('__') + '_' + path;
          parent = parent.parentNode;
        }

        return path;
      }
    }, {
      key: "suggestId",
      value: function suggestId(id) {
        if (document.querySelector('#' + id) !== null) {
          var nums = id.match(/.*(\d+)/);

          if (nums !== null) {
            var num = nums[1];
            var name = id.replace(num, '');
            id = this.suggestId(name + (++num).toString());
          } else {
            id = this.suggestId(id + '2');
          }
        }

        return id;
      }
    }, {
      key: "genElId",
      value: function genElId(el) {
        var id = el.getAttribute('id');

        if (!id) {
          var domPath = this.genElPathString(el);
          id = this.suggestId(domPath);
        }

        return id;
      }
    }, {
      key: "mountTemplate",
      value: function mountTemplate(path, id, hostEl, preRenData) {
        var _this3 = this;

        var tplHash = path + JSON.stringify(preRenData);

        if (!this.templateLoad[tplHash]) {
          this.templateLoad[tplHash] = new Promise(function (resolve, reject) {
            var _this4 = this;

            _newArrowCheck(this, _this3);

            var el = this.findTemplateEl(id, hostEl);

            if (el !== null) {
              var elInstance = document.importNode(el, true); // can be mustache or native template inlined and overriden

              if (this.variableRender && this.templateEngine && preRenData) {
                elInstance.innerHTML = this.templateEngine.render(elInstance.innerHTML, preRenData);
              }

              resolve(elInstance);
            } else {
              this.loadTemplate(path).then(function (body) {
                _newArrowCheck(this, _this4);

                var template = this.findTemplateEl(id, hostEl);

                if (!template) {
                  if (this.tplFmt === 'handlebars') {
                    template = this.createEl('script');
                    template.setAttribute('type', 'text/x-handlebars-template');
                  } else {
                    template = this.createEl('template');
                  }
                }

                template.setAttribute('id', id);
                template.innerHTML = body;

                if (this.cacheTemplates) {
                  el = this.findTemplateEl(id, hostEl);

                  if (this.allowGlobalTemplates) {
                    document.body.appendChild(template);
                  } else {
                    hostEl.appendChild(template);
                  }

                  el = this.findTemplateEl(id, hostEl);
                }

                if (this.variableRender && this.templateEngine && preRenData) {
                  var contents = this.templateEngine.render(body, preRenData);
                  var templateCopy = document.importNode(template, true);
                  templateCopy.innerHTML = contents;
                  resolve(templateCopy);
                } else {
                  resolve(el);
                }
              }.bind(this));
            }
          }.bind(this));
        }

        return this.templateLoad[tplHash] || Promise.reject('Template load error', tplHash);
      }
    }, {
      key: "prepareTemplate",
      value: function prepareTemplate(tpl) {
        if (this.tplFmt && this.tplFmt === 'handlebars') {
          var el = this.jquery(tpl.innerHTML);
          var html = '';

          if (el.length > 0) {
            html = this.jquery().prop ? this.jquery(tpl.innerHTML).prop('outerHTML') : this.jquery(tpl.innerHTML)[0].outerHTML;
          }

          var wrapper = this.createEl('div');
          this.jquery(wrapper).append(html);
          return wrapper.firstElementChild;
        } else {
          return document.importNode(tpl.content, true);
        }
      }
    }, {
      key: "renderTemplate",
      value: function renderTemplate(sl, dataMap) {
        var template = this.queryTemplate(sl);
        var fragment = document.importNode(template.content, true);

        for (var _i2 = 0, _Object$keys2 = Object.keys(dataMap); _i2 < _Object$keys2.length; _i2++) {
          var _sl = _Object$keys2[_i2];
          var entry = dataMap[_sl];
          var el = fragment.querySelector(_sl);

          if (el !== null) {
            if (entry.type === 'attr') {
              el.setAttribute(entry.attr, entry.value);
            } else {
              el.textContent = entry.value;
            }
          } else {
            console.warn("Element with selector ".concat(_sl, " not found in DOM"));
          }
        }

        return fragment;
      }
    }, {
      key: "renderMustacheVars",
      value: function renderMustacheVars(el, map) {
        var wrapper = this.createEl('div');
        wrapper.appendChild(el);
        var template = wrapper.outerHTML.toString(); // :TODO detect if document-fragment or element is passed

        for (var _i3 = 0, _Object$keys3 = Object.keys(map); _i3 < _Object$keys3.length; _i3++) {
          var key = _Object$keys3[_i3];
          template = template.replace(new RegExp('{{ ' + key + ' }}', 'g'), map[key]);
        }

        var wrapper2 = this.createEl('div');
        wrapper2.insertAdjacentHTML('beforeend', template);
        return wrapper2.firstElementChild.innerHTML;
      }
    }, {
      key: "createEl",
      value: function createEl(tagName, options) {
        return document.createElement(tagName, options);
      }
    }, {
      key: "renderWithTr",
      value: function renderWithTr(trJson, targetEl) {
        var _this5 = this;

        var fakeRoot = this.createEl('div');
        fakeRoot.appendChild(targetEl);
        fakeRoot.querySelectorAll('[tr]').forEach(function (el) {
          _newArrowCheck(this, _this5);

          var tr = el.getAttribute('tr');

          if (tr === 'contents') {
            var trValue = trJson[el.innerHTML];

            if (trValue !== undefined) {
              el.innerHTML = trValue;
            }
          } else if (tr.startsWith('attr')) {
            var trInfo = tr.split(':');
            var _trValue = trJson[trInfo[2]];

            if (_trValue !== undefined) {
              el.setAttribute(trInfo[1], _trValue);
            }
          }
        }.bind(this));
        return fakeRoot.firstChild.cloneNode(true);
      }
    }, {
      key: "localizeEl",
      value: function localizeEl(targetEl, locale) {
        var _this6 = this;

        var self = this;
        return new Promise(function (resolve, reject) {
          _newArrowCheck(this, _this6);

          if (!this.translations[locale]) {
            fetch(self.basePath + '/theme/' + this.theme + '/i18n/' + 'gridy-i18n-' + locale + '.json').then(function (response) {
              var _this7 = this;

              response.json().then(function (json) {
                _newArrowCheck(this, _this7);

                self.translations[locale] = json;
                resolve(self.renderWithTr(self.translations[locale], targetEl));
              }.bind(this));
            });
          } else {
            resolve(self.renderWithTr(this.translations[locale], targetEl));
          }
        }.bind(this));
      }
    }, {
      key: "templateEngine",
      get: function get() {
        if (!this._templateEngine) {
          if (this.variableRender === 'handlebars') {
            if (this.Handlebars !== undefined) {
              this._templateEngine = new HgTemplateEngine$1(this.Handlebars);
            } else {
              if (window && window['Handlebars']) {
                this._templateEngine = new HgTemplateEngine$1(window['Handlebars']);
              } else {
                console.error('no handlebars found in window or property');
              }
            }
          } else {
            if (typeof window[this.variableRender] === 'function') {
              window[this.variableRender].call(this);
            } else {
              this._templateEngine = new BaseTemplateEngine$1();
            }
          }
        }

        return this._templateEngine;
      }
    }], [{
      key: "hasRenderAttrs",
      value: function hasRenderAttrs(el) {
        for (var _i4 = 0, _Object$keys4 = Object.keys(RD_SHARED_ATTRS$1); _i4 < _Object$keys4.length; _i4++) {
          var attrName = _Object$keys4[_i4];
          var value = el.getAttribute(attrName);

          if (value !== null && value !== undefined) {
            return true;
          }
        }

        return false;
      }
    }, {
      key: "configureForElement",
      value: function configureForElement(el) {
        if (!el.renderer) {
          el.renderer = new Renderer();
          el.renderer.confFromEl(el);
        } else {
          // if one of renderer options specified element needs own extended renderer
          if (Renderer.hasRenderAttrs(el)) {
            var rendererCopy = el.renderer;
            el.renderer = new Renderer();

            for (var _i5 = 0, _Object$keys5 = Object.keys(RD_SHARED_ATTRS$1); _i5 < _Object$keys5.length; _i5++) {
              var attrName = _Object$keys5[_i5];

              if (rendererCopy[RD_SHARED_ATTRS$1[attrName]] !== null && rendererCopy[RD_SHARED_ATTRS$1[attrName]] !== undefined) {
                el.renderer[RD_SHARED_ATTRS$1[attrName]] = rendererCopy[RD_SHARED_ATTRS$1[attrName]];
              }
            }

            el.renderer.confFromEl(el);
          }
        }
      }
    }]);

    return Renderer;
  }();

  var SkTheme = function SkTheme(configEl) {
    _classCallCheck(this, SkTheme);

    this.configEl = configEl;
  };

  var DefaultTheme = /*#__PURE__*/function (_SkTheme) {
    _inherits(DefaultTheme, _SkTheme);

    var _super = _createSuper(DefaultTheme);

    function DefaultTheme() {
      _classCallCheck(this, DefaultTheme);

      return _super.apply(this, arguments);
    }

    _createClass(DefaultTheme, [{
      key: "basePath",
      get: function get() {
        if (!this._basePath) {
          this._basePath = this.configEl ? "".concat(this.configEl.basePath, "/theme/default") : '/node_modules/skinny-widgets/theme/default';
        }

        return this._basePath;
      }
    }, {
      key: "styles",
      get: function get() {
        if (!this._styles) {
          this._styles = {
            'default.css': "".concat(this.basePath, "/default.css")
          };
        }

        return this._styles;
      }
    }]);

    return DefaultTheme;
  }(SkTheme);

  var AntdTheme = /*#__PURE__*/function (_SkTheme) {
    _inherits(AntdTheme, _SkTheme);

    var _super = _createSuper(AntdTheme);

    function AntdTheme() {
      _classCallCheck(this, AntdTheme);

      return _super.apply(this, arguments);
    }

    _createClass(AntdTheme, [{
      key: "basePath",
      get: function get() {
        if (!this._basePath) {
          this._basePath = this.configEl ? "".concat(this.configEl.basePath, "/theme/antd") : '/node_modules/skinny-widgets/theme/antd';
        }

        return this._basePath;
      }
    }, {
      key: "styles",
      get: function get() {
        if (!this._styles) {
          this._styles = {
            'antd.css': "".concat(this.basePath, "/antd.min.css"),
            'antd-theme.css': "".concat(this.basePath, "/antd-theme.css")
          };
        }

        return this._styles;
      }
    }]);

    return AntdTheme;
  }(SkTheme);

  var JqueryTheme = /*#__PURE__*/function (_SkTheme) {
    _inherits(JqueryTheme, _SkTheme);

    var _super = _createSuper(JqueryTheme);

    function JqueryTheme() {
      _classCallCheck(this, JqueryTheme);

      return _super.apply(this, arguments);
    }

    _createClass(JqueryTheme, [{
      key: "basePath",
      get: function get() {
        if (!this._basePath) {
          this._basePath = this.configEl ? "".concat(this.configEl.basePath, "/theme/jquery") : '/node_modules/skinny-widgets/theme/jquery';
        }

        return this._basePath;
      }
    }, {
      key: "styles",
      get: function get() {
        if (!this._styles) {
          this._styles = {
            'jquery-ui.css': "".concat(this.basePath, "/jquery-ui.css"),
            'theme.css': "".concat(this.basePath, "/theme.css"),
            'jquery-theme.css': "".concat(this.basePath, "/jquery-theme.css")
          };
        }

        return this._styles;
      }
    }]);

    return JqueryTheme;
  }(SkTheme);

  var SkThemes = /*#__PURE__*/function () {
    function SkThemes() {
      _classCallCheck(this, SkThemes);
    }

    _createClass(SkThemes, null, [{
      key: "byName",
      value: function byName(name, config) {
        var theme = new DefaultTheme(config);

        if (name === 'antd') {
          theme = new AntdTheme(config);
        } else if (name === 'jquery') {
          theme = new JqueryTheme(config);
        }

        return theme;
      }
    }]);

    return SkThemes;
  }();

  var SkLocaleEn = {};

  var SkLocaleRu = {
    'Field value is invalid': 'Ошибка ввода данных',
    'This field is required': 'Это поле обязательное',
    'The value is less than required': 'Значение меньше допустимого',
    'The value is greater than required': 'Значение больше допустимого',
    'The value must me a valid email': 'Значение должно быть email адресом',
    'The value must match requirements': 'Значение должно соответствовать'
  };

  var SkLocaleCn = {
    'Field value is invalid': '不对',
    'This field is required': '必填',
    'The value is less than required': '太小',
    'The value is greater than required': '太大',
    'The value must me a valid email': '需要电子邮件',
    'The value must match requirements': '不对'
  };

  var LANGS_BY_CODES = {
    'en_US': 'EN',
    'en_UK': 'EN',
    'en': 'EN',
    'EN': 'EN',
    'us': 'EN',
    'ru_RU': 'RU',
    'ru': 'RU',
    'cn': 'CN',
    'CN': 'CN',
    'zh': 'CN',
    'de': 'DE',
    'DE': 'DE'
  };
  var LOCALES = {
    'EN': SkLocaleEn,
    'RU': SkLocaleRu,
    'CN': SkLocaleCn
  };
  var SkLocale = /*#__PURE__*/function () {
    function SkLocale(lang) {
      _classCallCheck(this, SkLocale);

      this.lang = LANGS_BY_CODES[lang];
    }

    _createClass(SkLocale, [{
      key: "tr",
      value: function tr(string) {
        if (this.lang) {
          return LOCALES[this.lang][string] ? LOCALES[this.lang][string] : string;
        } else {
          return SkLocaleEn[string] ? SkLocaleEn[string] : string;
        }
      }
    }]);

    return SkLocale;
  }();

  var SkElement = /*#__PURE__*/function (_HTMLElement) {
    _inherits(SkElement, _HTMLElement);

    var _super = _createSuper(SkElement);

    function SkElement() {
      _classCallCheck(this, SkElement);

      return _super.apply(this, arguments);
    }

    _createClass(SkElement, [{
      key: "onReflection",
      value: function onReflection(event) {
        var _this = this;

        console.debug('sk-element configChanged', event);
        this.configEl.reconfigureElement(this, event.detail);

        if (this.impl.contentsState) {
          this.contentsState = JSON.parse(JSON.stringify(this.impl.contentsState));
        }

        if (event.detail.name && event.detail.name === 'theme') {
          this.impl.unmountStyles();
          this.impl.unbindEvents();
          this.impl.clearTplCache();
          this.impl = null;
        }

        this.addEventListener('rendered', function (event) {
          _newArrowCheck(this, _this);

          this.impl.restoreState({
            contentsState: this.contentsState
          });
        }.bind(this));
        this.render();
      }
    }, {
      key: "setupRenderer",
      value: function setupRenderer() {
        // if no renderer were injected, check if we can use shared by config renderer
        if (!this.renderer) {
          if (this.configEl && !this.configEl.hasAttribute('rd-no-shrd')) {
            if (this.configEl.renderer) {
              this.renderer = this.configEl.renderer;
            }
          }
        } // instanciate new render if no one were provided before or element tag has render options defined


        Renderer$1.configureForElement(this);
      }
    }, {
      key: "setup",
      value: function setup() {
        this.setupRenderer();

        if (this.configEl && this.configEl.configureElement) {
          this.configEl.configureElement(this);

          if (this.reflective && this.reflective !== 'false') {
            this.configEl.addEventListener('configChanged', this.onReflection.bind(this));
          }
        }

        if (this.useShadowRoot) {
          if (!this.shadowRoot) {
            this.attachShadow({
              mode: 'open'
            });
          }
        }
      }
    }, {
      key: "render",
      value: function () {
        var _render = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
          var _this2 = this;

          var onRender;
          return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  onRender = this.impl.render();
                  onRender.then(function () {
                    _newArrowCheck(this, _this2);

                    this.impl.bindEvents();
                    this.bindAutoRender();
                    this.dispatchEvent(new CustomEvent('rendered'));
                  }.bind(this));

                case 2:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee, this);
        }));

        function render() {
          return _render.apply(this, arguments);
        }

        return render;
      }()
    }, {
      key: "css2CamelCase",
      value: function css2CamelCase(str) {
        return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function (word, index) {
          return index === 0 ? word.toLowerCase() : word.toUpperCase();
        }).replace(/\s+/g, '').replace('-', '');
      }
    }, {
      key: "capitalizeFirstLetter",
      value: function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
      }
    }, {
      key: "bindByCn",
      value: function bindByCn(attrName, el, type) {
        if (attrName != null) {
          var propName = this.css2CamelCase(attrName);
          console.log('propName', propName);
          var descriptor = Object.getOwnPropertyDescriptor(this, propName);

          if (descriptor) {
            Object.defineProperty(this, propName, {
              set: function (value) {
                descriptor.set(value);

                if (type === 'at') {
                  if (propName.indexOf('_') > 0) {
                    var _propName$split = propName.split('_'),
                        _propName$split2 = _slicedToArray(_propName$split, 2),
                        source = _propName$split2[0],
                        target = _propName$split2[1];

                    el.setAttribute(target, value);
                  } else {
                    el.setAttribute(propName, value);
                  }
                } else if (type === 'cb') {
                  var callbackName = 'on' + this.capitalizeFirstLetter(propName) + 'Change';

                  if (typeof this[callbackName] === 'function') {
                    this[callbackName].call(this, el, value);
                  }
                } else {
                  el.innerHTML = value;
                }

                this['_' + propName] = value;
              }.bind(this),
              get: function () {
                return this['_' + propName];
              }.bind(this),
              configurable: true
            });
          } else {
            Object.defineProperty(this, propName, {
              set: function (value) {
                if (type === 'at') {
                  if (propName.indexOf('_') > 0) {
                    var _propName$split3 = propName.split('_'),
                        _propName$split4 = _slicedToArray(_propName$split3, 2),
                        source = _propName$split4[0],
                        target = _propName$split4[1];

                    el.setAttribute(target, value);
                  } else {
                    el.setAttribute(propName, value);
                  }
                } else if (type === 'cb') {
                  var callbackName = 'on' + this.capitalizeFirstLetter(propName) + 'Change';

                  if (typeof this[callbackName] === 'function') {
                    this[callbackName].call(this, el, value);
                  }
                } else {
                  el.innerHTML = value;
                }

                this['_' + propName] = value;
              }.bind(this),
              get: function () {
                return this['_' + propName];
              }.bind(this),
              configurable: true
            });
          }
        }
      }
    }, {
      key: "bindAutoRender",
      value: function bindAutoRender(target) {
        target = target ? target : this.el;
        var autoRenderedEls = target.querySelectorAll('[class*="sk-prop-"]');

        var _iterator = _createForOfIteratorHelper(autoRenderedEls),
            _step;

        try {
          for (_iterator.s(); !(_step = _iterator.n()).done;) {
            var autoEl = _step.value;
            // bind in and at classed elements contents and attrs to element's attributes or class props
            // if class property with name matched found we bind it
            // if class property with name matched not found we define and bind it
            var inNames = autoEl.className.match('sk-prop-in-(\\S+)');

            if (inNames) {
              // :TODO validate and transform name to oroper attr or prop name
              this.bindByCn(inNames[1], autoEl);
            }

            var atNames = autoEl.className.match('sk-prop-at-(\\S+)');

            if (atNames) {
              // :TODO validate and transform name to oroper attr or prop name
              this.bindByCn(atNames[1], autoEl, 'at');
            }

            var cbNames = autoEl.className.match('sk-prop-cb-(\\S+)');

            if (cbNames) {
              // :TODO validate and transform name to oroper attr or prop name
              this.bindByCn(cbNames[1], autoEl, 'cb');
            }
          }
        } catch (err) {
          _iterator.e(err);
        } finally {
          _iterator.f();
        }
      }
    }, {
      key: "connectedCallback",
      value: function connectedCallback() {
        this.setup();
        this.render();
      }
    }, {
      key: "setCustomValidity",
      value: function setCustomValidity(validity) {
        this.validationMessage = validity;
        this.impl.renderValidation(validity);
      }
    }, {
      key: "flushValidity",
      value: function flushValidity() {
        this.impl.flushValidation();
      }
    }, {
      key: "theme",
      get: function get() {
        return this.getAttribute('theme') || 'default';
      },
      set: function set(theme) {
        return this.setAttribute('theme', theme);
      }
    }, {
      key: "basePath",
      get: function get() {
        // defaulting for packaged library usage
        return this.getAttribute('base-path') || '/node_modules/skinny-widgets/src';
      },
      set: function set(basePath) {
        return this.setAttribute('base-path', basePath);
      }
    }, {
      key: "useShadowRoot",
      get: function get() {
        if (this.getAttribute('use-shadow-root') === 'false') {
          return false;
        }

        return true;
      }
    }, {
      key: "lang",
      get: function get() {
        return this.getAttribute('lang') || 'en_US';
      }
    }, {
      key: "locale",
      get: function get() {
        if (!this._locale) {
          this._locale = new SkLocale(this.lang);
        }

        return this._locale;
      }
    }, {
      key: "reflective",
      get: function get() {
        return this.getAttribute('reflective') || true;
      }
    }, {
      key: "el",
      get: function get() {
        return this.useShadowRoot ? this.shadowRoot : this;
      }
    }, {
      key: "contentsState",
      set: function set(state) {
        this.setAttribute('state', JSON.stringify(state));
      },
      get: function get() {
        return JSON.parse(this.getAttribute('state'));
      }
    }, {
      key: "configSl",
      get: function get() {
        return this.getAttribute('config-sl') || this.getAttribute('configSl') || 'sk-config';
      }
    }, {
      key: "configEl",
      get: function get() {
        if (!this._configEl) {
          this._configEl = document.querySelector(this.configSl);
        }

        return this._configEl;
      },
      set: function set(configEl) {
        this._configEl = configEl;
      }
    }, {
      key: "renderer",
      get: function get() {
        return this._renderer;
      },
      set: function set(renderer) {
        this._renderer = renderer;
      }
    }, {
      key: "skTheme",
      get: function get() {
        return SkThemes.byName(this.theme, this);
      }
    }, {
      key: "validationMessage",
      get: function get() {
        return this._validationMessage || this.locale.tr('Field value is invalid');
      },
      set: function set(validationMessage) {
        this._validationMessage = validationMessage;
      }
    }]);

    return SkElement;
  }( /*#__PURE__*/_wrapNativeSuper(HTMLElement));

  /**
   * elements than can have action attribute to be binded with event or callback, currently in sk-dialog and sk-form only
   * @type {string}
   */
  var ACTION_SUBELEMENTS_SL = 'button,sk-button';
  var SkComponentImpl = /*#__PURE__*/function (_EventTarget) {
    _inherits(SkComponentImpl, _EventTarget);

    var _super = _createSuper(SkComponentImpl);

    _createClass(SkComponentImpl, [{
      key: "tplPath",
      get: function get() {
        if (!this._tplPath) {
          this._tplPath = "".concat(this.themePath, "/").concat(this.prefix, "-sk-").concat(this.suffix, ".tpl.html");
        }

        return this._tplPath;
      }
    }, {
      key: "themePath",
      get: function get() {
        var basePath = this.comp.basePath.toString();

        if (basePath.endsWith('/')) {
          // remove traling slash
          basePath = basePath.substr(0, basePath.length - 1);
        }

        return "".concat(basePath, "/theme/").concat(this.comp.theme);
      }
    }]);

    function SkComponentImpl(comp) {
      var _this;

      _classCallCheck(this, SkComponentImpl);

      _this = _super.call(this);
      _this.comp = comp;
      return _this;
    }

    _createClass(SkComponentImpl, [{
      key: "bindEvents",
      value: function bindEvents() {}
    }, {
      key: "unbindEvents",
      value: function unbindEvents() {}
    }, {
      key: "dumpState",
      value: function dumpState() {}
    }, {
      key: "restoreState",
      value: function restoreState(state) {}
    }, {
      key: "enable",
      value: function enable() {}
    }, {
      key: "disable",
      value: function disable() {}
    }, {
      key: "afterRendered",
      value: function afterRendered() {
        this.dispatchEvent(new CustomEvent('skafterrendered'));
      }
    }, {
      key: "beforeRendered",
      value: function () {
        var _beforeRendered = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
          return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  this.dispatchEvent(new CustomEvent('skbeforerendered'));

                case 1:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee, this);
        }));

        function beforeRendered() {
          return _beforeRendered.apply(this, arguments);
        }

        return beforeRendered;
      }()
    }, {
      key: "unmountStyles",
      value: function unmountStyles() {}
    }, {
      key: "findStyle",
      value: function findStyle(seek) {
        var links = document.querySelectorAll('link');
        var matched = [];

        var _iterator = _createForOfIteratorHelper(links),
            _step;

        try {
          for (_iterator.s(); !(_step = _iterator.n()).done;) {
            var link = _step.value;

            if (Array.isArray(seek)) {
              var _iterator2 = _createForOfIteratorHelper(seek),
                  _step2;

              try {
                for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
                  var seekItem = _step2.value;

                  if (link.href.match(seekItem)) {
                    matched.push(link);
                    break;
                  }
                }
              } catch (err) {
                _iterator2.e(err);
              } finally {
                _iterator2.f();
              }
            } else {
              if (link.href.match(seek)) {
                matched.push(link);
              }
            }
          }
        } catch (err) {
          _iterator.e(err);
        } finally {
          _iterator.f();
        }

        return matched;
      }
    }, {
      key: "attachStyleByName",
      value: function attachStyleByName(name, target) {
        if (!target) {
          target = this.comp.el;
        }

        var path = this.comp.configEl.styles[name];

        if (path) {
          var link = document.createElement('link');
          link.setAttribute('rel', 'stylesheet');
          link.setAttribute('href', path);
          target.appendChild(link);
        } else {
          console.warn('style not found with config el', name);
        }
      }
    }, {
      key: "attachStyleByPath",
      value: function attachStyleByPath(path, target) {
        if (!target) {
          target = this.comp.el;
        }

        if (target.querySelector("link[href='".concat(path, "']")) == null) {
          var link = document.createElement('link');
          link.setAttribute('rel', 'stylesheet');
          link.setAttribute('href', path);
          target.appendChild(link);
        }
      }
    }, {
      key: "mountStyles",
      value: function mountStyles() {
        this.mountedStyles = this.mountedStyles || {};

        if (this.comp.skTheme) {
          var styles = this.comp.skTheme.styles;
          var configStyles = this.comp.configEl && this.comp.configEl.styles ? Object.keys(this.comp.configEl.styles) : [];

          for (var _i = 0, _Object$keys = Object.keys(styles); _i < _Object$keys.length; _i++) {
            var styleName = _Object$keys[_i];
            var href = configStyles.includes(styleName) ? this.comp.configEl.styles[styleName] : styles[styleName];

            if (!this.mountedStyles[href]) {
              // mount only not mounted before
              var link = document.createElement('link');
              link.setAttribute('rel', 'stylesheet');
              link.setAttribute('href', href);
              this.comp.el.appendChild(link);
              this.mountedStyles[href] = link;
              console.log('mounted style: ', href, ' to component: ', this.comp.tagName);
            }
          }
        }
      }
    }, {
      key: "mountStyle",
      value: function mountStyle(fileNames, target) {
        if (!target) {
          target = this.comp.el;
        }

        if (this.comp.useShadowRoot) {
          if (this.comp.configEl && this.comp.configEl.styles) {
            if (Array.isArray(fileNames)) {
              var _iterator3 = _createForOfIteratorHelper(fileNames),
                  _step3;

              try {
                for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
                  var name = _step3.value;
                  this.attachStyleByName(name, target);
                }
              } catch (err) {
                _iterator3.e(err);
              } finally {
                _iterator3.f();
              }
            } else {
              this.attachStyleByName(fileNames, target);
            }
          } else {
            var styles = this.findStyle(fileNames);

            if (styles && styles.length > 0) {
              var style = styles[0];

              if (style) {
                target.appendChild(document.importNode(style));
              } else {
                console.warn('style not found', fileNames);
              }
            }
          }
        }
      }
    }, {
      key: "clearTplCache",
      value: function clearTplCache() {
        if (this.cachedTplId) {
          var tplEl = document.querySelector('#' + this.cachedTplId);

          if (tplEl !== null) {
            tplEl.remove();
          }
        }
      }
    }, {
      key: "idGenEnabled",
      value: function idGenEnabled() {
        return this.comp.configEl && this.comp.configEl.hasAttribute('gen-ids') && this.comp.configEl.hasAttribute('gen-ids') !== 'false';
      }
    }, {
      key: "render",
      value: function () {
        var _render = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
          var id, el, rendered;
          return regeneratorRuntime.wrap(function _callee2$(_context2) {
            while (1) {
              switch (_context2.prev = _context2.next) {
                case 0:
                  if (!this.comp.getAttribute('id') && this.idGenEnabled()) {
                    id = this.comp.renderer.genElId(this.comp);
                    this.comp.setAttribute('id', id);
                  } else {
                    id = this.comp.getAttribute('id');
                  }

                  this.comp.tpl = this.comp.renderer.findTemplateEl(this.cachedTplId, this.comp); // try to find overriden template

                  if (this.comp.tpl) {
                    _context2.next = 6;
                    break;
                  }

                  _context2.next = 5;
                  return this.comp.renderer.mountTemplate(this.tplPath, this.cachedTplId, this.comp, {
                    themePath: this.themePath
                  });

                case 5:
                  this.comp.tpl = _context2.sent;

                case 6:
                  _context2.next = 8;
                  return this.beforeRendered();

                case 8:
                  el = this.comp.renderer.prepareTemplate(this.comp.tpl);
                  rendered = this.comp.renderer.renderMustacheVars(el, {
                    id: id,
                    themePath: this.themePath
                  });
                  this.comp.el.innerHTML = '';
                  this.comp.el.innerHTML = rendered;
                  this.indexMountedStyles();
                  this.afterRendered();

                case 14:
                case "end":
                  return _context2.stop();
              }
            }
          }, _callee2, this);
        }));

        function render() {
          return _render.apply(this, arguments);
        }

        return render;
      }()
    }, {
      key: "indexMountedStyles",
      value: function indexMountedStyles() {
        var links = this.comp.el.querySelectorAll('link');
        this.mountedStyles = {};

        var _iterator4 = _createForOfIteratorHelper(links),
            _step4;

        try {
          for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
            var link = _step4.value;
            this.mountedStyles[link.getAttribute('href')] = link;
          }
        } catch (err) {
          _iterator4.e(err);
        } finally {
          _iterator4.f();
        }
      }
    }, {
      key: "findParent",
      value: function findParent(el, tag) {
        while (el.parentNode) {
          el = el.parentNode;
          if (el.tagName === tag) return el;
        }

        return null;
      }
    }, {
      key: "validateWithAttrs",
      value: function validateWithAttrs() {
        if (this.skFormParent) {
          // if has sk-form parents, rely on it
          return true;
        }

        this.hasOwnValidator = false;
        var validatorNames = Object.keys(this.skValidators);

        for (var _i2 = 0, _validatorNames = validatorNames; _i2 < _validatorNames.length; _i2++) {
          var validatorName = _validatorNames[_i2];

          // :TODO check there is no parent sk-form first
          if (this.comp.hasAttribute(validatorName)) {
            this.hasOwnValidator = true;
            var validator = this.skValidators[validatorName];
            var result = validator.validate(this.comp);
            return result;
          }
        }

        if (!this.hasOwnValidator) {
          // any value is valid if no validator defined
          return true;
        }

        return false;
      }
    }, {
      key: "showInvalid",
      value: function showInvalid() {
        if (this.comp.hasAttribute('validation-label') && this.comp.getAttribute('validation-label') !== 'disabled') {
          this.renderValidation();
        }
      }
    }, {
      key: "showValid",
      value: function showValid() {
        if (this.comp.hasAttribute('validation-label') && this.comp.getAttribute('validation-label') !== 'disabled') {
          this.flushValidation();
        }
      }
    }, {
      key: "renderValidation",
      value: function renderValidation(validity) {
        if (this.comp.hasAttribute('validation-label') && this.comp.getAttribute('validation-label') !== 'disabled') {
          var msgEl = this.comp.el.querySelector('.form-validation-message');

          if (!msgEl) {
            msgEl = this.comp.renderer.createEl('span');
            msgEl.classList.add('form-validation-message');
            msgEl.insertAdjacentHTML('afterbegin', validity || this.comp.validationMessage);
            this.comp.el.appendChild(msgEl);
          } else {
            msgEl.innerHTML = '';
            msgEl.insertAdjacentHTML('afterbegin', validity || this.comp.validationMessage);
          }
        }
      }
    }, {
      key: "flushValidation",
      value: function flushValidation() {
        var msgEl = this.comp.el.querySelector('.form-validation-message');

        if (msgEl) {
          if (msgEl.parentElement) {
            msgEl.parentElement.removeChild(msgEl);
          } else {
            msgEl.parentNode.removeChild(msgEl);
          }
        }
      }
    }, {
      key: "getScrollTop",
      value: function getScrollTop() {
        if (typeof pageYOffset != 'undefined') {
          return pageYOffset;
        } else {
          var body = _document.body;
          var _document = _document.documentElement;
          _document = _document.clientHeight ? _document : body;
          return _document.scrollTop;
        }
      }
    }, {
      key: "bindAction",
      value: function bindAction(button, action) {
        if (this['on' + action]) {
          // handler defined on custom element
          button.onclick = function (event) {
            this['on' + action]();
            this.comp.dispatchEvent(new CustomEvent(action, {
              target: this.comp,
              detail: {
                value: event.target.value
              },
              bubbles: true,
              composed: true
            }));
          }.bind(this);
        } else if (this.comp['on' + action]) {
          // handler defined in implementation
          button.onclick = function (event) {
            this.comp['on' + action]();
            this.comp.dispatchEvent(new CustomEvent(action, {
              target: this.comp,
              detail: {
                value: event.target.value
              },
              bubbles: true,
              composed: true
            }));
          }.bind(this);
        } else {
          // no handler binded just throw and event
          button.onclick = function (event) {
            this.comp.dispatchEvent(new CustomEvent(action, {
              target: this.comp,
              detail: {
                value: event.target.value
              },
              bubbles: true,
              composed: true
            }));
          }.bind(this);
        }
      }
    }, {
      key: "bindActions",
      value: function bindActions(target) {
        target = target ? target : this;
        var buttons = target.querySelectorAll(this.actionSubElementsSl);

        var _iterator5 = _createForOfIteratorHelper(buttons),
            _step5;

        try {
          for (_iterator5.s(); !(_step5 = _iterator5.n()).done;) {
            var button = _step5.value;

            if (button.hasAttribute('action')) {
              var action = button.getAttribute('action');
              this.bindAction(button, action);
            }

            if (button.dataset.action) {
              var _action = button.dataset.action;
              this.bindAction(button, _action);
            }
          }
        } catch (err) {
          _iterator5.e(err);
        } finally {
          _iterator5.f();
        }
      }
    }, {
      key: "cachedTplId",
      get: function get() {
        return this.comp.constructor.name + 'Tpl';
      }
    }, {
      key: "skFormParent",
      get: function get() {
        if (this._skFormParent === undefined) {
          this._skFormParent = this.findParent(this.comp, 'SK-FORM');
        }

        return this._skFormParent;
      }
    }, {
      key: "actionSubElementsSl",
      get: function get() {
        if (!this._actionSubElementsSl) {
          this._actionSubElementsSl = ACTION_SUBELEMENTS_SL;
        }

        return this._actionSubElementsSl;
      },
      set: function set(sl) {
        this._actionSubElementsSl = sl;
      }
    }]);

    return SkComponentImpl;
  }( /*#__PURE__*/_wrapNativeSuper(EventTarget));

  var SkDialogImpl = /*#__PURE__*/function (_SkComponentImpl) {
    _inherits(SkDialogImpl, _SkComponentImpl);

    var _super = _createSuper(SkDialogImpl);

    function SkDialogImpl() {
      _classCallCheck(this, SkDialogImpl);

      return _super.apply(this, arguments);
    }

    _createClass(SkDialogImpl, [{
      key: "renderTitle",
      value: function renderTitle() {
        if (this.comp.title) {
          this.headerEl.style.display = 'block';
          this.titleEl.innerHTML = this.comp.title;
        } else {
          this.titleEl.style.display = 'none';
        }
      }
    }, {
      key: "dumpState",
      value: function dumpState() {
        return {
          'contents': this.comp.contentsState
        };
      }
    }, {
      key: "unmountStyles",
      value: function unmountStyles() {
        _get(_getPrototypeOf(SkDialogImpl.prototype), "unmountStyles", this).call(this);

        if (this.styles && this.styles.length > 0) {
          var _iterator = _createForOfIteratorHelper(this.styles),
              _step;

          try {
            for (_iterator.s(); !(_step = _iterator.n()).done;) {
              var style = _step.value;
              var path = this.comp.configEl.styles[style];
              var link = document.body.querySelector("link[href=\"".concat(path, "\"]"));

              if (link !== null) {
                document.body.removeChild(link);
              }
            }
          } catch (err) {
            _iterator.e(err);
          } finally {
            _iterator.f();
          }
        }
      }
    }, {
      key: "restoreState",
      value: function restoreState(state) {
        this.body.innerHTML = '';
        this.body.insertAdjacentHTML('beforeend', state.contentsState);
      }
    }, {
      key: "cleanFooterTplCache",
      value: function cleanFooterTplCache() {
        var oldTpl = document.getElementById(this.comp.constructor.name + 'FooterTpl');

        if (oldTpl !== null) {
          oldTpl.remove();
        }
      }
    }, {
      key: "renderFooter",
      value: function () {
        var _renderFooter = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
          var tpl, el;
          return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  this.cleanFooterTplCache();
                  tpl = this.comp.renderer.findTemplateEl(this.comp.constructor.name + 'FooterTpl', this.dialog); // try to find overriden template

                  if (tpl) {
                    _context.next = 6;
                    break;
                  }

                  _context.next = 5;
                  return this.comp.renderer.mountTemplate(this.tplFooterPath, this.comp.constructor.name + 'FooterTpl', this.comp, {});

                case 5:
                  tpl = _context.sent;

                case 6:
                  el = this.comp.renderer.prepareTemplate(tpl);
                  this.footerEl.innerHTML = '';
                  this.footerEl.appendChild(el);
                  this.bindActions(this.footerEl);

                case 10:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee, this);
        }));

        function renderFooter() {
          return _renderFooter.apply(this, arguments);
        }

        return renderFooter;
      }()
    }, {
      key: "remountToBody",
      value: function remountToBody(dialog) {
        this.dialog = document.body.appendChild(dialog);
        var styles = Object.keys(this.comp.skTheme.styles);

        if (styles) {
          var _iterator2 = _createForOfIteratorHelper(styles),
              _step2;

          try {
            for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
              var style = _step2.value;
              this.attachStyleByPath(this.comp.skTheme.styles[style], document.body);
            }
          } catch (err) {
            _iterator2.e(err);
          } finally {
            _iterator2.f();
          }
        }

        this.comp.bindAutoRender(this.dialog);
        this.bindActions(this.dialog);
      }
    }, {
      key: "polyfillNativeDialog",
      value: function polyfillNativeDialog(dialog) {
        if (typeof this.dialog.showModal !== "function") {
          if (!window.dialogPolyfill) {
            console.log('you trying to polyfill native dialog element, but polyfill was not loaded');
            return false;
          }

          this.remountToBody(dialog);
          window.dialogPolyfill.registerDialog(this.dialog);
          this.dialogPolyfilled = true;
        } else {
          if (this.comp.hasAttribute('to-body')) {
            this.remountToBody(this.dialog);
          }
        }
      }
    }, {
      key: "getScrollTop",
      value: function getScrollTop() {
        if (typeof pageYOffset !== 'undefined') {
          return pageYOffset;
        } else {
          var body = _document.body;
          var _document = _document.documentElement;
          _document = _document.clientHeight ? _document : body;
          return _document.scrollTop;
        }
      }
    }, {
      key: "open",
      value: function open() {
        this.dialog.style.setProperty('display', 'block', 'important');

        if (typeof this.dialog.showModal === "function") {
          if (!this.dialog.hasAttribute('open')) {
            this.dialog.showModal();
          } else {
            console.warn('trying to open dialog with open attr');
          }
        } else {
          console.warn('native dialog not supported by this browser');
        }

        if (this.dialogPolyfilled) {
          var box = this.dialog.getBoundingClientRect();
          this.dialog.style.position = 'fixed';
          this.dialog.style.left = (window.outerWidth - box.width) / 2 + 'px';
          this.dialog.style.top = (window.outerHeight - box.height) / 2 + 'px';
          this.dialog.style.margin = 0;
          this.dialog.style.padding = 0;
        }
      }
    }, {
      key: "close",
      value: function close() {
        this.dialog.style.setProperty('display', 'none', 'important');

        if (typeof this.dialog.close === "function") {
          if (this.dialog.hasAttribute('open')) {
            this.dialog.close();
          } else {
            console.warn('attempt to close dialog w/o open attr');
          }
        } else {
          console.warn('native dialog not supported by this browser');
        }
      }
    }, {
      key: "render",
      value: function () {
        var _render = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
          var id, ownTemplates, tplClones, _iterator3, _step3, _template, el, rendered, dialog, _i, _tplClones, template;

          return regeneratorRuntime.wrap(function _callee2$(_context2) {
            while (1) {
              switch (_context2.prev = _context2.next) {
                case 0:
                  if (!this.comp.getAttribute('id') && this.idGenEnabled()) {
                    id = this.comp.renderer.genElId(this.comp);
                    this.comp.setAttribute('id', id);
                  } else {
                    id = this.comp.getAttribute('id');
                  }

                  ownTemplates = this.comp.querySelectorAll('template');
                  tplClones = [];
                  _iterator3 = _createForOfIteratorHelper(ownTemplates);

                  try {
                    for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
                      _template = _step3.value;
                      tplClones.push(_template.cloneNode(true));
                    }
                  } catch (err) {
                    _iterator3.e(err);
                  } finally {
                    _iterator3.f();
                  }

                  this.comp.tpl = this.comp.renderer.findTemplateEl(this.cachedTplId, this.comp); // try to find overriden template

                  if (this.comp.tpl) {
                    _context2.next = 10;
                    break;
                  }

                  _context2.next = 9;
                  return this.comp.renderer.mountTemplate(this.tplPath, this.cachedTplId, this.comp, {
                    themePath: this.themePath
                  });

                case 9:
                  this.comp.tpl = _context2.sent;

                case 10:
                  _context2.next = 12;
                  return this.beforeRendered();

                case 12:
                  el = this.comp.renderer.prepareTemplate(this.comp.tpl);
                  rendered = this.comp.renderer.renderMustacheVars(el, {
                    id: id,
                    themePath: this.themePath
                  });
                  dialog = this.comp.renderer.createEl('dialog');
                  dialog.setAttribute('id', id + 'Dialog');
                  dialog.insertAdjacentHTML('beforeend', rendered);

                  for (_i = 0, _tplClones = tplClones; _i < _tplClones.length; _i++) {
                    template = _tplClones[_i];
                    dialog.appendChild(template);
                  }

                  if (this.comp.hasAttribute('to-body')) {
                    this.dialog = document.body.appendChild(dialog);
                  } else {
                    this.comp.el.innerHTML = '';
                    this.comp.el.appendChild(dialog);
                    this.indexMountedStyles();
                  }

                  this.afterRendered();

                case 20:
                case "end":
                  return _context2.stop();
              }
            }
          }, _callee2, this);
        }));

        function render() {
          return _render.apply(this, arguments);
        }

        return render;
      }()
    }]);

    return SkDialogImpl;
  }(SkComponentImpl);

  var AntdSkDialog = /*#__PURE__*/function (_SkDialogImpl) {
    _inherits(AntdSkDialog, _SkDialogImpl);

    var _super = _createSuper(AntdSkDialog);

    function AntdSkDialog() {
      _classCallCheck(this, AntdSkDialog);

      return _super.apply(this, arguments);
    }

    _createClass(AntdSkDialog, [{
      key: "restoreState",
      value: function restoreState(state) {
        this.body.innerHTML = '';
        this.body.insertAdjacentHTML('beforeend', state.contentsState);
      }
    }, {
      key: "beforeRendered",
      value: function () {
        var _beforeRendered = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
          return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  _get(_getPrototypeOf(AntdSkDialog.prototype), "beforeRendered", this).call(this);

                  if (this.comp.useShadowRoot) {
                    this.contentsState = this.comp.el.host.innerHTML;
                    this.comp.el.host.innerHTML = '';
                  } else {
                    this.contentsState = this.comp.el.innerHTML;
                    this.comp.el.innerHTML = '';
                  }

                case 2:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee, this);
        }));

        function beforeRendered() {
          return _beforeRendered.apply(this, arguments);
        }

        return beforeRendered;
      }()
    }, {
      key: "afterRendered",
      value: function () {
        var _afterRendered = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
          return regeneratorRuntime.wrap(function _callee2$(_context2) {
            while (1) {
              switch (_context2.prev = _context2.next) {
                case 0:
                  _get(_getPrototypeOf(AntdSkDialog.prototype), "afterRendered", this).call(this);

                  this.restoreState({
                    contentsState: this.contentsState || this.comp.contentsState
                  });
                  this.close();
                  this.renderTitle();

                  if (this.comp.type === 'confirm' || this.comp.type === 'action') {
                    this.renderFooter();
                  } else {
                    this.footerEl.style.display = 'none';
                  }

                  this.bindActions(this.dialog);
                  this.mountStyles();

                case 7:
                case "end":
                  return _context2.stop();
              }
            }
          }, _callee2, this);
        }));

        function afterRendered() {
          return _afterRendered.apply(this, arguments);
        }

        return afterRendered;
      }()
    }, {
      key: "bindEvents",
      value: function bindEvents() {
        _get(_getPrototypeOf(AntdSkDialog.prototype), "bindEvents", this).call(this);

        this.polyfillNativeDialog(this.dialog);

        this.closeBtn.onclick = function (event) {
          this.close(event);
        }.bind(this);
      }
    }, {
      key: "unbindEvents",
      value: function unbindEvents() {
        _get(_getPrototypeOf(AntdSkDialog.prototype), "unbindEvents", this).call(this);

        this.closeBtn.onclick = null;
      }
    }, {
      key: "prefix",
      get: function get() {
        return 'antd';
      }
    }, {
      key: "suffix",
      get: function get() {
        return 'dialog';
      }
    }, {
      key: "tplFooterPath",
      get: function get() {
        if (!this._tplFooterPath) {
          this._tplFooterPath = "".concat(this.comp.basePath, "/theme/").concat(this.comp.theme, "/").concat(this.prefix, "-sk-").concat(this.suffix, "-footer.tpl.html");
        }

        return this._tplFooterPath;
      }
    }, {
      key: "dialog",
      get: function get() {
        if (!this._dialog) {
          this._dialog = this.comp.el.querySelector('dialog');
        }

        return this._dialog;
      },
      set: function set(dialog) {
        this._dialog = dialog;
      }
    }, {
      key: "body",
      get: function get() {
        return this.dialog.querySelector('.ant-modal-body');
      }
    }, {
      key: "closeBtn",
      get: function get() {
        return this.dialog.querySelector('.ant-modal-close');
      }
    }, {
      key: "headerEl",
      get: function get() {
        return this.dialog.querySelector('.ant-modal-header');
      }
    }, {
      key: "footerEl",
      get: function get() {
        return this.dialog.querySelector('.ant-modal-footer');
      }
    }, {
      key: "titleEl",
      get: function get() {
        if (!this._titleEl) {
          this._titleEl = this.dialog.querySelector('.ant-modal-title');
        }

        return this._titleEl;
      }
    }, {
      key: "btnCancelEl",
      get: function get() {
        return this.dialog.querySelector('.btn-cancel');
      }
    }, {
      key: "btnConfirmEl",
      get: function get() {
        return this.dialog.querySelector('.btn-confirm');
      }
    }]);

    return AntdSkDialog;
  }(SkDialogImpl);

  var DefaultSkDialog = /*#__PURE__*/function (_SkDialogImpl) {
    _inherits(DefaultSkDialog, _SkDialogImpl);

    var _super = _createSuper(DefaultSkDialog);

    function DefaultSkDialog() {
      _classCallCheck(this, DefaultSkDialog);

      return _super.apply(this, arguments);
    }

    _createClass(DefaultSkDialog, [{
      key: "bindEvents",
      value: function bindEvents() {
        _get(_getPrototypeOf(DefaultSkDialog.prototype), "bindEvents", this).call(this);

        this.polyfillNativeDialog(this.dialog);
      }
    }, {
      key: "beforeRendered",
      value: function () {
        var _beforeRendered = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
          return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  _get(_getPrototypeOf(DefaultSkDialog.prototype), "beforeRendered", this).call(this);

                  if (this.comp.useShadowRoot) {
                    this.contentsState = this.comp.el.host.innerHTML;
                    this.comp.el.host.innerHTML = '';
                  } else {
                    this.contentsState = this.comp.el.innerHTML;
                    this.comp.el.innerHTML = '';
                  }

                case 2:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee, this);
        }));

        function beforeRendered() {
          return _beforeRendered.apply(this, arguments);
        }

        return beforeRendered;
      }()
    }, {
      key: "afterRendered",
      value: function afterRendered() {
        _get(_getPrototypeOf(DefaultSkDialog.prototype), "afterRendered", this).call(this);

        this.dialog.querySelector('.contents').insertAdjacentHTML('beforeend', this.contentsState);
        this.renderTitle();

        if (this.comp.type === 'confirm') {
          this.renderFooter();
        }

        this.mountStyles();
      }
    }, {
      key: "open",
      value: function open() {
        this.dialog.showModal();
      }
    }, {
      key: "close",
      value: function close() {
        this.dialog.close();
      }
    }, {
      key: "renderTitle",
      value: function renderTitle() {
        if (this.comp.title) {
          this.headerEl.style.display = 'block';
          this.headerEl.innerHTML = this.comp.title;
        } else {
          this.headerEl.style.display = 'none';
        }
      }
    }, {
      key: "styles",
      get: function get() {
        return ['default.css'];
      }
    }, {
      key: "prefix",
      get: function get() {
        return 'default';
      }
    }, {
      key: "suffix",
      get: function get() {
        return 'dialog';
      }
    }, {
      key: "btnCancelEl",
      get: function get() {
        return this.dialog.querySelector('.btn-cancel');
      }
    }, {
      key: "btnConfirmEl",
      get: function get() {
        return this.dialog.querySelector('.btn-confirm');
      }
    }, {
      key: "tplFooterPath",
      get: function get() {
        return "".concat(this.comp.basePath, "/theme/").concat(this.comp.theme, "/").concat(this.prefix, "-sk-").concat(this.suffix, "-footer.tpl.html");
      }
    }, {
      key: "headerEl",
      get: function get() {
        return this.dialog.querySelector('.header');
      }
    }, {
      key: "dialog",
      get: function get() {
        if (!this._dialog) {
          this._dialog = this.comp.el.querySelector('dialog');
        }

        return this._dialog;
      },
      set: function set(dialog) {
        this._dialog = dialog;
      }
    }, {
      key: "body",
      get: function get() {
        return this.dialog.querySelector('.contents');
      }
    }, {
      key: "footerEl",
      get: function get() {
        return this.dialog.querySelector('.footer');
      }
    }]);

    return DefaultSkDialog;
  }(SkDialogImpl);

  var JquerySkDialog = /*#__PURE__*/function (_SkDialogImpl) {
    _inherits(JquerySkDialog, _SkDialogImpl);

    var _super = _createSuper(JquerySkDialog);

    function JquerySkDialog() {
      _classCallCheck(this, JquerySkDialog);

      return _super.apply(this, arguments);
    }

    _createClass(JquerySkDialog, [{
      key: "restoreState",
      value: function restoreState(state) {
        this.body.innerHTML = '';
        this.body.insertAdjacentHTML('beforeend', state.contentsState);
      }
    }, {
      key: "beforeRendered",
      value: function () {
        var _beforeRendered = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
          return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  _get(_getPrototypeOf(JquerySkDialog.prototype), "beforeRendered", this).call(this);

                  if (this.comp.useShadowRoot) {
                    this.contentsState = this.comp.el.host.innerHTML;
                    this.comp.el.host.innerHTML = '';
                  } else {
                    this.contentsState = this.comp.el.innerHTML;
                    this.comp.el.innerHTML = '';
                  }

                case 2:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee, this);
        }));

        function beforeRendered() {
          return _beforeRendered.apply(this, arguments);
        }

        return beforeRendered;
      }()
    }, {
      key: "afterRendered",
      value: function () {
        var _afterRendered = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
          return regeneratorRuntime.wrap(function _callee2$(_context2) {
            while (1) {
              switch (_context2.prev = _context2.next) {
                case 0:
                  _get(_getPrototypeOf(JquerySkDialog.prototype), "afterRendered", this).call(this);

                  this.restoreState({
                    contentsState: this.contentsState || this.comp.contentsState
                  });
                  this.close();
                  this.renderTitle();

                  if (this.comp.type === 'confirm') {
                    this.renderFooter();
                  }

                  this.bindActions(this.dialog);
                  this.mountStyles();

                case 7:
                case "end":
                  return _context2.stop();
              }
            }
          }, _callee2, this);
        }));

        function afterRendered() {
          return _afterRendered.apply(this, arguments);
        }

        return afterRendered;
      }()
    }, {
      key: "bindEvents",
      value: function bindEvents() {
        _get(_getPrototypeOf(JquerySkDialog.prototype), "bindEvents", this).call(this);

        this.polyfillNativeDialog(this.dialog);

        this.closeBtn.onclick = function (event) {
          this.close(event);
        }.bind(this);
      }
    }, {
      key: "unbindEvents",
      value: function unbindEvents() {
        _get(_getPrototypeOf(JquerySkDialog.prototype), "unbindEvents", this).call(this);

        this.closeBtn.removeEventListener('click', this.close);
      }
    }, {
      key: "prefix",
      get: function get() {
        return 'jquery';
      }
    }, {
      key: "suffix",
      get: function get() {
        return 'dialog';
      }
    }, {
      key: "tplFooterPath",
      get: function get() {
        return "".concat(this.comp.basePath, "/theme/").concat(this.comp.theme, "/").concat(this.prefix, "-sk-").concat(this.suffix, "-footer.tpl.html");
      }
    }, {
      key: "dialog",
      get: function get() {
        if (!this._dialog) {
          this._dialog = this.comp.el.querySelector('dialog');
        }

        return this._dialog;
      },
      set: function set(dialog) {
        this._dialog = dialog;
      }
    }, {
      key: "body",
      get: function get() {
        return this.dialog.querySelector('.ui-dialog-content');
      }
    }, {
      key: "closeBtn",
      get: function get() {
        return this.dialog.querySelector('.ui-dialog-titlebar-close');
      }
    }, {
      key: "footerEl",
      get: function get() {
        return this.dialog.querySelector('.ui-dialog-footer');
      }
    }, {
      key: "headerEl",
      get: function get() {
        return this.dialog.querySelector('.ui-dialog-titlebar');
      }
    }, {
      key: "titleEl",
      get: function get() {
        return this.dialog.querySelector('.ui-dialog-title');
      }
    }, {
      key: "btnCancelEl",
      get: function get() {
        return this.dialog.querySelector('.btn-cancel');
      }
    }, {
      key: "btnConfirmEl",
      get: function get() {
        return this.dialog.querySelector('.btn-confirm');
      }
    }]);

    return JquerySkDialog;
  }(SkDialogImpl);

  var SkDialog = /*#__PURE__*/function (_SkElement) {
    _inherits(SkDialog, _SkElement);

    var _super = _createSuper(SkDialog);

    function SkDialog() {
      _classCallCheck(this, SkDialog);

      return _super.apply(this, arguments);
    }

    _createClass(SkDialog, [{
      key: "open",
      value: function open() {
        this.impl.open();
      }
    }, {
      key: "close",
      value: function close() {
        this.impl.close();
      }
    }, {
      key: "toggle",
      value: function toggle() {
        if (this.open) {
          this.impl.close();
        } else {
          this.impl.open();
        }
      }
    }, {
      key: "oncancel",
      value: function oncancel() {
        this.close();
      }
    }, {
      key: "impl",
      get: function get() {
        if (!this._impl) {
          if (this.theme === 'antd') {
            this.impl = new AntdSkDialog(this);
          } else if (this.theme === 'jquery') {
            this.impl = new JquerySkDialog(this);
          } else {
            this.impl = new DefaultSkDialog(this);
          }
        }

        return this._impl;
      },
      set: function set(impl) {
        this._impl = impl;
      }
    }, {
      key: "type",
      get: function get() {
        return this.getAttribute('type');
      }
    }]);

    return SkDialog;
  }(SkElement);

  var FmConfigDialog = /*#__PURE__*/function (_SkDialog) {
    _inherits(FmConfigDialog, _SkDialog);

    var _super = _createSuper(FmConfigDialog);

    function FmConfigDialog() {
      _classCallCheck(this, FmConfigDialog);

      return _super.apply(this, arguments);
    }

    _createClass(FmConfigDialog, [{
      key: "loadSettings",
      value: function () {
        var _loadSettings = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
          return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  return _context.abrupt("return", loadFromUrl(this.settingsUrl));

                case 1:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee, this);
        }));

        function loadSettings() {
          return _loadSettings.apply(this, arguments);
        }

        return loadSettings;
      }()
    }, {
      key: "connectedCallback",
      value: function connectedCallback() {
        var _this = this;

        _get(_getPrototypeOf(FmConfigDialog.prototype), "connectedCallback", this).call(this);

        this.loadSettings().then(function (settings) {
          _newArrowCheck(this, _this);

          this.settings = settings;
        }.bind(this));
        this.addEventListener('save', function (event) {
          _newArrowCheck(this, _this);

          this.saveParams();
          this.close();
        }.bind(this));
      }
    }, {
      key: "saveParams",
      value: function () {
        var _saveParams = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
          var _this2 = this;

          var dataTokens, _i, _Object$keys, settingName, el, value, xhr;

          return regeneratorRuntime.wrap(function _callee2$(_context2) {
            while (1) {
              switch (_context2.prev = _context2.next) {
                case 0:
                  dataTokens = [];

                  if (this.settings) {
                    _context2.next = 5;
                    break;
                  }

                  _context2.next = 4;
                  return this.loadSettings();

                case 4:
                  this.settings = _context2.sent;

                case 5:
                  for (_i = 0, _Object$keys = Object.keys(this.settings); _i < _Object$keys.length; _i++) {
                    settingName = _Object$keys[_i];

                    if (settingName !== 'l11n') {
                      el = this.impl.dialog.querySelector("[name=".concat(settingName, "]"));

                      if (el != null) {
                        value = el.value;
                        dataTokens.push(settingName + '=' + encodeURIComponent(btoa(value)));
                      }
                    }
                  }

                  xhr = new XMLHttpRequest();
                  xhr.open("POST", this.saveUrl, true);
                  xhr.setRequestHeader("Content-Type", 'application/x-www-form-urlencoded');
                  xhr.send(dataTokens.join('&'));

                  xhr.onreadystatechange = function (res) {
                    _newArrowCheck(this, _this2);

                    if (res.readyState == XMLHttpRequest.DONE) {
                      if (res.status == 200) {
                        this.onSaved(res);
                      }
                    }
                  }.bind(this);

                case 11:
                case "end":
                  return _context2.stop();
              }
            }
          }, _callee2, this);
        }));

        function saveParams() {
          return _saveParams.apply(this, arguments);
        }

        return saveParams;
      }()
    }, {
      key: "onSaved",
      value: function onSaved(res) {
        console.log('onSaved', res);
      }
    }, {
      key: "ns",
      get: function get() {
        return this.getAttribute('ns') || '';
      }
    }, {
      key: "saveUrl",
      get: function get() {
        return this.getAttribute('saveUrl') || '';
      }
    }, {
      key: "settingsUrl",
      get: function get() {
        return this.getAttribute('settingsUrl') || '';
      }
    }]);

    return FmConfigDialog;
  }(SkDialog);

  var FmCartConfigDialog = /*#__PURE__*/function (_FmConfigDialog) {
    _inherits(FmCartConfigDialog, _FmConfigDialog);

    var _super = _createSuper(FmCartConfigDialog);

    function FmCartConfigDialog() {
      _classCallCheck(this, FmCartConfigDialog);

      return _super.apply(this, arguments);
    }

    return FmCartConfigDialog;
  }(FmConfigDialog);

  var CartButtonElement = /*#__PURE__*/function (_HTMLElement) {
    _inherits(CartButtonElement, _HTMLElement);

    var _super = _createSuper(CartButtonElement);

    _createClass(CartButtonElement, [{
      key: "stickyOffset",
      // eventBus: EventBus;
      get: function get() {
        return this.getAttribute('sticky-offset') || '10px';
      }
    }]);

    function CartButtonElement() {
      var _this;

      _classCallCheck(this, CartButtonElement);

      _this = _super.call(this);
      console.log('cart button element ctor');
      return _this;
    }

    _createClass(CartButtonElement, [{
      key: "getScrollTop",
      value: function getScrollTop() {
        if (typeof pageYOffset != 'undefined') {
          return pageYOffset;
        } else {
          var body = _document.body;
          var _document = _document.documentElement;
          _document = _document.clientHeight ? _document : body;
          return _document.scrollTop;
        }
      }
    }, {
      key: "remountToBody",
      value: function remountToBody(dialog) {
        this.dialog = document.body.appendChild(dialog.cloneNode(true));
        dialog.parentNode.removeChild(dialog);
      }
    }, {
      key: "showCart",
      value: function showCart() {
        this.cartDialog.open();
      }
    }, {
      key: "connectedCallback",
      value: function connectedCallback() {
        this.renderTemplate(); //this.loadSettings();
        // :TODO optional get cart dialog link/selector from attribute

        this.cartDialog = document.querySelector('#' + this.ns + 'cartDialog');

        if (this.cartDialog == null) {
          document.body.appendChild("<cart-dialog id=\"".concat(this.ns, "cartDialog\" class=\"my-modal\"></cart-dialog>"));
          this.cartDialog = document.querySelector('#' + this.ns + 'cartDialog');
        }

        if (!this.cartShowHandle) {
          this.cartShowHandle = this.showCart.bind(this);
          this.eventBus.addEventListener('cart:show', this.cartShowHandle);

          if (this.hasAttribute('sticky') && this.getAttribute('sticky') !== 'false') {
            this.bindSticky();
          }
        }
      }
    }, {
      key: "bindSticky",
      value: function bindSticky() {
        var rect = this.getBoundingClientRect();
        window.addEventListener('scroll', function (event) {
          var scrollTop = this.getScrollTop();
          var y = this.offsetHeight + this.offsetTop;

          if (scrollTop > y) {
            this.style.position = 'fixed';
            this.style.top = this.stickyOffset;
            this.style.zIndex = '999';
            this.style.left = rect.left.toString() + 'px';
          } else {
            this.style.removeProperty('position');
            this.style.removeProperty('top');
            this.style.removeProperty('zIndex');
            this.style.removeProperty('left');
          }
        }.bind(this));
      }
    }, {
      key: "loadSettings",
      value: function loadSettings() {
        return loadFromUrl(this.settingsUrl);
      }
    }, {
      key: "updateItemCounter",
      value: function updateItemCounter() {
        var _this2 = this;

        var cntItem = this.querySelector('#' + this.ns + 'cartButtonItemCount');

        if (cntItem != null) {
          if (this.cartService.count > 0) {
            cntItem.textContent = this.cartService.count;
            this.classList.remove('fm-cart-empty');
            this.classList.add('fm-cart-not-empty');
          } else {
            cntItem.textContent = 0;
            this.classList.add('fm-cart-empty');
            this.classList.remove('fm-cart-not-empty');
          }
        }

        this.classList.add('fm-cart-added');
        setTimeout(function () {
          _newArrowCheck(this, _this2);

          this.classList.remove('fm-cart-added');
        }.bind(this), 1000);
      }
    }, {
      key: "renderTemplate",
      value: function renderTemplate(settings) {
        this.template = document.getElementById(this.ns + 'template-cart-button');
        this.appendChild(this.template.content);
        this.cartService.bindUpdate(this.updateItemCounter.bind(this));
        this.updateItemCounter();
      }
    }, {
      key: "ns",
      get: function get() {
        return this.getAttribute('ns') || '';
      }
    }, {
      key: "settingsUrl",
      get: function get() {
        return this.getAttribute('settingsUrl') || '';
      }
    }, {
      key: "placeOrderUrl",
      get: function get() {
        return this.getAttribute('placeOrderUrl') || '';
      }
    }]);

    return CartButtonElement;
  }( /*#__PURE__*/_wrapNativeSuper(HTMLElement));

  var CartDialog = /*#__PURE__*/function (_SkDialog) {
    _inherits(CartDialog, _SkDialog);

    var _super = _createSuper(CartDialog);

    function CartDialog() {
      _classCallCheck(this, CartDialog);

      return _super.apply(this, arguments);
    }

    _createClass(CartDialog, [{
      key: "open",
      value: function open() {
        var cartList = this.impl.dialog.querySelector('cart-list');

        if (cartList) {
          cartList.dispatchEvent(new CustomEvent('cart-list:refresh'));
        }

        _get(_getPrototypeOf(CartDialog.prototype), "open", this).call(this);
      }
    }]);

    return CartDialog;
  }(SkDialog);

  exports.CartButtonElement = CartButtonElement;
  exports.CartDialog = CartDialog;
  exports.CartListElement = CartListElement;
  exports.CartService = CartService;
  exports.EventBus = EventBus;
  exports.FmCartConfigDialog = FmCartConfigDialog;
  exports.Registry = Registry;
  exports.Renderer = Renderer;

  Object.defineProperty(exports, '__esModule', { value: true });

})));
