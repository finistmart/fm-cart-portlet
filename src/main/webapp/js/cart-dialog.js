
import { SkDialog } from "../node_modules/skinny-widgets/src/sk-dialog.js";




export class CartDialog extends SkDialog {
    open() {
        let cartList = this.impl.dialog.querySelector('cart-list');
        if (cartList) {
            cartList.dispatchEvent(new CustomEvent('cart-list:refresh'));
        }
        super.open();
    }
}