
import { loadFromUrl } from './../node_modules/complets/prefs.js';

export class CartConfigElement extends HTMLElement {


    constructor() {
        super();
        console.log('cart config element ctor');
    }


    loadSettings() {
        return loadFromUrl(this.settingsUrl);
    }

    connectedCallback() {
        this.loadSettings().then((settings) => {
            this.settings = settings;
        });
        this.renderTemplate();
        this.bindActions();

    }

    renderTemplate() {
        this.template = document.getElementById(this.ns + 'template-cart-config');
        this.appendChild(this.template.content);
    }

    open() {
        this.configDialog.showModal();
    }

    bindActions() {

        this.closeButton = this.querySelector('#' + this.ns + 'closeDialogBtn');
        this.configDialog = this.querySelector('#' + this.ns + 'configDialog');


        this.closeButton.addEventListener('click', (event) => {
            this.configDialog.close();
        });

        this.saveButton = this.querySelector('#' + this.ns + 'saveDialogBtn');
        this.saveButton.addEventListener('click', (event) => {
            this.saveParams();
        });
    }

    saveParams() {
        let dataTokens = [];
        for (let settingName of Object.keys(this.settings)) {
            if (settingName != 'l11n') {
                let el = this.querySelector(`input[name=${settingName}]`);
                if (el != null) {
                    let value = el.value;
                    dataTokens.push(settingName + '=' + encodeURIComponent(btoa(value)));
                }
            }
        }

        let xhr = new XMLHttpRequest();
        xhr.open("POST", this.saveUrl, true);
        xhr.setRequestHeader("Content-Type", 'application/x-www-form-urlencoded');
        xhr.send(dataTokens.join('&'));

        xhr.onreadystatechange = (res) => {
            if(res.readyState == XMLHttpRequest.DONE) {
                if (res.status == 200) {
                    this.onSaved(res);
                }
            }
        }
    }

    onSaved(res) {
        console.log('onSaved', res);
    }

    get ns() {
        return this.getAttribute('ns') || '';
    }

    get saveUrl() {
        return this.getAttribute('saveUrl') || '';
    }

    get settingsUrl() {
        return this.getAttribute('settingsUrl') || '';
    }
}
