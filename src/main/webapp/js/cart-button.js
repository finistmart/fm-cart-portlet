

import { loadFromUrl } from './../node_modules/complets/prefs.js';


export class CartButtonElement extends HTMLElement {

    // eventBus: EventBus;

    get stickyOffset() {
        return this.getAttribute('sticky-offset') || '10px';
    }

    constructor() {
        super();
        console.log('cart button element ctor');


    }

    getScrollTop() {
        if (typeof pageYOffset != 'undefined') {
            return pageYOffset;
        } else {
            let body = document.body;
            let document = document.documentElement;
            document = (document.clientHeight) ? document : body;
            return document.scrollTop;
        }
    }

    remountToBody(dialog) {
        this.dialog = document.body.appendChild(dialog.cloneNode(true));
        dialog.parentNode.removeChild(dialog);
    }

    showCart() {
        this.cartDialog.open();
    }

    connectedCallback() {
        this.renderTemplate();
        //this.loadSettings();
        // :TODO optional get cart dialog link/selector from attribute
        this.cartDialog = document.querySelector('#' + this.ns + 'cartDialog');
        if (this.cartDialog == null) {
            document.body.appendChild(`<cart-dialog id="${this.ns}cartDialog" class="my-modal"></cart-dialog>`);
            this.cartDialog = document.querySelector('#' + this.ns + 'cartDialog');
        }
        if (! this.cartShowHandle) {
            this.cartShowHandle = this.showCart.bind(this);
            this.eventBus.addEventListener('cart:show', this.cartShowHandle);
            if (this.hasAttribute('sticky') && this.getAttribute('sticky') !== 'false') {
                this.bindSticky();
            }
        }
    }

    bindSticky() {
        let rect = this.getBoundingClientRect();
        window.addEventListener('scroll', function (event) {
            let scrollTop = this.getScrollTop();
            let y = this.offsetHeight + this.offsetTop;
            if (scrollTop > y) {
                this.style.position = 'fixed';
                this.style.top = this.stickyOffset;
                this.style.zIndex = '999';
                this.style.left = (rect.left).toString() + 'px';
            } else {
                this.style.removeProperty('position');
                this.style.removeProperty('top');
                this.style.removeProperty('zIndex');
                this.style.removeProperty('left');
            }
        }.bind(this));
    }

    loadSettings() {

        return loadFromUrl(this.settingsUrl);
    }

    updateItemCounter() {
        let cntItem = this.querySelector('#' + this.ns + 'cartButtonItemCount');
        if (cntItem != null) {
            if (this.cartService.count > 0) {
                cntItem.textContent = this.cartService.count;
                this.classList.remove('fm-cart-empty');
                this.classList.add('fm-cart-not-empty');
            } else {
                cntItem.textContent = 0;
                this.classList.add('fm-cart-empty');
                this.classList.remove('fm-cart-not-empty');
            }
        }
        this.classList.add('fm-cart-added');
        setTimeout(() => {
            this.classList.remove('fm-cart-added');
        }, 1000);
    }

    renderTemplate(settings) {
        this.template = document.getElementById(this.ns + 'template-cart-button');

        this.appendChild(this.template.content);
        this.cartService.bindUpdate(this.updateItemCounter.bind(this));
        this.updateItemCounter();

    }


    get ns() {
        return this.getAttribute('ns') || '';
    }

    get settingsUrl() {
        return this.getAttribute('settingsUrl') || '';
    }
    get placeOrderUrl() {
        return this.getAttribute('placeOrderUrl') || '';
    }
}
