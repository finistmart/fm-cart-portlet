

export class CartService {


    constructor() {
        console.log('cart service element ctor');
        let storedItems = JSON.parse(sessionStorage.getItem('fm-cart-items'));
        this.items = storedItems || {};
        this.delivery = {};
        this.callbacks = [];
    }

    get count() {
        return Object.keys(this.items).length;
    }

    addItem(event) {
        console.log('addItem', event);
        let itemToAdd = event.detail.item;
        let qty = itemToAdd.qty || 1;
        let keys = Object.keys(this.items);
        let itemExists = false;
        for (let itemKey of keys) {
            let item = this.items[itemKey];
            if (item.title == itemToAdd.title) {
                this.items[itemKey].qty += qty;
                itemExists = true;
            }
        }
        if (!itemExists) {
            itemToAdd['qty'] = qty;
            this.items[Object.keys(this.items).length] = itemToAdd;
        }
        this.applyCallbacks();
        sessionStorage.setItem('fm-cart-items', JSON.stringify(this.items));
    }

    applyCallbacks() {
        this.callbacks.forEach((callback, i) => {
            callback();
        });
    }

    bindUpdate(callback) {
        this.callbacks.push(callback);
    }

    placeOrder(url) {
        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest();
            xhr.open("POST", url);
            xhr.setRequestHeader("Content-Type", 'application/x-www-form-urlencoded');
            xhr.onreadystatechange = (event) => {
                if (event.target.readyState == XMLHttpRequest.DONE) {
                    if (event.target.status == 200) {
                        this.items = {};
                        this.applyCallbacks();
                        resolve();
                    } else {
                        reject();
                    }
                }
            };

            xhr.send('delivery=' + JSON.stringify(this.delivery) + '&items=' + JSON.stringify(this.items));
        });
    }

    clear() {
        this.items = [];
        sessionStorage.setItem('fm-cart-items', JSON.stringify(this.items));
    }

    removeItem(itemToDel) {
        console.log('removeItem', itemToDel);
        let keys = Object.keys(this.items);
        for (let itemKey of keys) {
            let item = this.items[itemKey];
            if (item.title == itemToDel.title) {
                delete this.items[itemKey];
            }
        }
        this.applyCallbacks();
        sessionStorage.setItem('fm-cart-items', JSON.stringify(this.items));
    }

    bindEvents(ns) {
        if (ns == null || ns == undefined) {
            ns = ''; //bind as global
        }
        document.addEventListener(ns + 'cart:additem', this.addItem.bind(this));
        document.addEventListener(ns + 'cart:removeitem', this.removeItem.bind(this));
        document.addEventListener(ns + 'cart:clear', this.clear.bind(this));
    }
}
