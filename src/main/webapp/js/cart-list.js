
import { loadFromUrl } from './../node_modules/complets/prefs.js';
import { Renderer } from './../node_modules/complets/renderer.js';
//import { inject } from './../node_modules/complets/registry.js';

import { CartService } from './cart-service.js';

export class CartListElement extends HTMLElement {

    //renderer: Renderer;

    //cartService: CartService;

    constructor() {
        super();
        console.log('cart list element ctor');

    }

    refresh() {
        if (this.settings) {
            this.renderTemplate(this.settings);
        }
    }

    onItemSelected() {
        document.dispatchEvent(new CustomEvent('cart-list:showdelivery'));
    }

    connectedCallback() {
        this.loadSettings().then(function(settings) {
            this.settings = settings;
            this.renderTemplate(settings);
            if (! this.eventsBinded) {
                if (! window.showDeliveryHandler) {
                    window.showDeliveryHandler = this.renderDeliveryForm.bind(this);
                    document.addEventListener('cart-list:showdelivery', window.showDeliveryHandler);
                }
                if (! window.itemSelectedHandler) {
                    window.itemSelectedHandler = this.onItemSelected.bind(this);
                    document.addEventListener('cart-list:itemsselected', window.itemSelectedHandler);
                }
                document.dispatchEvent(new CustomEvent('cart-list:rendered'));
                this.addEventListener('cart-list:refresh', (event) => {
                    this.refresh();
                });
                this.eventsBinded = true;
            }
        }.bind(this));
    }

    disconnectedCallback() {
        if (window.showDeliveryHandler) {
            document.removeEventListener('cart-list:showdelivery', this.showDeliveryHandler);
            delete window.showDeliveryHandler;
        }
        if (window.itemSelectedHandler) {
            document.removeEventListener('cart-list:itemsselected', this.itemSelectedHandler);
            delete window.itemSelectedHandler;
        }
        if (window.placeOrderHandler) {
            delete window.placeOrderHandler;
        }
    }

    loadSettings() {
        return loadFromUrl(this.settingsUrl);
    }

    translate(settings, el) {
        el = el ? el : this;
        let trElements = el.querySelectorAll('[class^=tr]');
        if (trElements !== null) {
            trElements.forEach((el, index) => {
                for (let className of el.classList) {
                    if (className.startsWith('tr-')) {
                        let normedClassName = className.replace(/-/g, '.');
                        let tr = this.settings.l11n[normedClassName];
                        if (tr) {
                            el.textContent = tr;
                        }
                    }
                }
            });
        }
    }

    renderTemplate(settings) {
        console.log('cart list settings', settings);
        this.template = document.getElementById(this.ns + 'template-cart-list');
        this.itemTemplate = document.getElementById(this.ns + 'template-cart-item');
        this.templateEmpty = document.getElementById(this.ns + 'template-cart-list-empty');
        this.innerHTML = '';
        let itemKeys = Object.keys(this.cartService.items);
        if (itemKeys.length > 0) {
            this.appendChild(document.importNode(this.template.content, true));
            let listContainer = this.querySelector('.fm-cart-container');
            if (listContainer !== null) {
                for (let no of Object.keys(this.cartService.items)) {
                    let cartItem = this.cartService.items[no];
                    let rowNum = parseInt(no) + 1;

                    let rowNode = this.renderer.renderTemplate('#' + this.ns + 'template-cart-item', {
                        '.fm-cart-no': { value: rowNum },
                        '.fm-cart-title': { value: cartItem.title },
                        '.fm-cart-price': { value: cartItem.price },
                        '.fm-cart-qty': { value: cartItem.qty },
                    });

                    rowNode.querySelector('.action-remove').addEventListener('click', this.removeItem.bind(this, cartItem));

                    listContainer.appendChild(rowNode);
                }
            }

            let buyBtn = this.querySelector('#' + this.ns + 'cartListBuyBtn');
            if (buyBtn !== null) {
                buyBtn.addEventListener('click', this.renderDeliveryForm.bind(this));
            }
        } else {
            this.appendChild(document.importNode(this.templateEmpty.content, true));
        }
        this.translate(settings);
    }

    valueByName(name) {
        let el = document.querySelector('[name=' + name + ']');
        if (el != null) {
            return el.value;
        } else {
            return null;
        }
    }

    processDeliveryForm() {
        if (! this.proceddDelivery) {
            this.proceddDelivery = new Promise((resolve, reject) => {
                let form = document.querySelector('#' + this.ns + 'deliveryForm');
                if (form !== null && form.checkValidity()) {
                    let data = {
                        email: this.valueByName('email'),
                        state: this.valueByName('state'),
                        city: this.valueByName('city'),
                        zipcode: this.valueByName('zipcode'),
                        address1: this.valueByName('address1'),
                        address2: this.valueByName('address2'),
                        fullName: this.valueByName('fullName'),
                        tel: this.valueByName('tel')
                    };
                    resolve(data);
                } else {
                    reject();
                }
            });
        }
        return this.proceddDelivery;
    }

    renderFinalMsg() {
        let templateFinalMsg = document.importNode(document.getElementById(this.ns + 'template_finalMsg'), true);
        //let body = this.cartDialog.impl.dialog.querySelector('.ant-modal-body');
        this.innerHTML = '';
        this.appendChild(templateFinalMsg.content);
        this.translate(this.settings, this.cartDialog);
        let okBtn = document.getElementById(this.ns + 'finalMsgOkBtn');
        okBtn.addEventListener('click', () => {
            document.dispatchEvent(new CustomEvent('cart:clear'));
            this.renderTemplate();
            this.cartDialog.close();
        });
    }

    onPlaceOrderClick(event) {
        event.preventDefault();
        event.stopPropagation();
        this.processDeliveryForm().then((data) => {
            this.cartService.delivery = data;
            this.placeOrder(this.placeOrderUrl).then(() => {
                this.renderFinalMsg();
            }, () => {
                console.error('error placing order')
            });
        }, () => {
            console.error('error validating order')
        });
    }

    renderDeliveryForm() {
        this.templateDelivery = document.getElementById(this.ns + 'template-delivery');
        this.innerHTML = '';
        this.appendChild(document.importNode(this.templateDelivery.content, true));

        this.translate(this.settings);
        this.cartDialog = document.getElementById(this.ns + 'cartDialog');
        let placeBtn = this.querySelector('#' + this.ns + 'placeOrderBtn');
        if (placeBtn !== null) {
            if (! window.placeOrderHandler) {
                window.placeOrderHandler = this.onPlaceOrderClick.bind(this);
                placeBtn.onclick = window.placeOrderHandler;
            }
        }
        let cancelBtn = this.querySelector('.cancel-modal-btn');
        if (cancelBtn !== null) {
            if (this.cancelBinded) {
                cancelBtn.addEventListener('click', (event) => {
                    event.preventDefault();
                    event.stopPropagation();
                    this.cartDialog.close();
                });
                this.cancelBinded = true;
            }
        }

    }

    placeOrder() {
        return this.cartService.placeOrder(this.placeOrderUrl);
    }

    removeItem(item) {
        this.cartService.removeItem(item);
        this.renderTemplate();
    }


    get ns() {
        return this.getAttribute('ns') || '';
    }

    get placeOrderUrl() {
        return this.getAttribute('placeOrderUrl') || '';
    }

    get settingsUrl() {
        return this.getAttribute('settingsUrl') || '';
    }
}
